﻿Shader "Custom/CurvedDiffused"
{
	Properties
	{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_Color("Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_QOffset("Offset", Vector) = (-10,-10,0,0)
		_Dist("Distance", Float) = 100.0
		_MinLighting("Minimal lighting", Float) = 0.3
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 150
		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _QOffset;
			float _Dist;
			float3 _LightColor0;
			float4 _Color;
			float _MinLighting;

			struct vertexInput
			{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float4 texcoord : TEXCOORD0;
			};

			struct fragmentInput
			{
				float4 pos : SV_POSITION;
				float4 uv : TEXCOORD0;
				float4 color : COLOR0;
			};

			fragmentInput vert(vertexInput i)
			{
				fragmentInput o;
				float4 vPos = mul(UNITY_MATRIX_MV, i.vertex);
				float zOff = vPos.z / _Dist;
				vPos += _QOffset*zOff*zOff;
				o.pos = mul(UNITY_MATRIX_P, vPos);
				o.uv = i.texcoord;

				float3 normalDirection = normalize(mul(float4(i.normal, 1.0), _World2Object).xyz);
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				float3 diffuse = _LightColor0.xyz * _Color.rgb * max(_MinLighting, dot(normalDirection, lightDirection));

				o.color = float4(diffuse, 1.0);

				return o;
			}

			half4 frag(fragmentInput i) : COLOR
			{
				return tex2D(_MainTex, i.uv.xy) * i.color;
			}
		
			ENDCG
		}
	}

	FallBack "Diffuse"
}
