﻿using UnityEngine;
using System.Collections;

public class CrazyModCoinController : MonoBehaviour 
{
    private Rigidbody coinRigidbody;

    private float counter;

    void Awake()
    {
        coinRigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        counter += Time.fixedDeltaTime;

        if(counter < 0.5f)
        {
            return;
        }
        counter = 0f;

        var x = Random.Range(-400f, 400f);
        var y = Random.Range(-400f, 400f);
        var z = Random.Range(-400f, 400f);

        coinRigidbody.AddForce(x, y, z);
    }
}