﻿using UnityEngine;

public class AIControl : MonoBehaviour 
{
    [SerializeField]
    private float AIUpdateInterval;
    [SerializeField]
    private float AIIntevalBetweenActions;
    [SerializeField]
    private float maxDistanceCheck;
    [SerializeField]
    private Vector3 raycastOffset;

    private BotInput botInput;
    private float AIScanTimer;
    private PlayerControl playerControl;

    void Awake()
    {
        playerControl = GetComponent<PlayerControl>();
        botInput = GetComponent<PlayerInput>() as BotInput;
    }

    void FixedUpdate()
    {
        if (GameStateManager.Instance.State != GameStates.InGame)
        {
            return;
        }

        if (AIScanTimer > 0)
        {
            AIScanTimer -= Time.fixedDeltaTime;

            return;
        }

        AIScanTimer = AIUpdateInterval;

        if (RayCollisionCheck(Vector3.forward) || RayCollisionCheck(Vector3.forward + Vector3.down))
        {
            ChangeLane();
        }
    }

    void ChangeLane()
    {
        if (!RayCollisionCheck(Vector3.left + Vector3.forward) &&
            !RayCollisionCheck(Vector3.left) && 
            playerControl.CanMoveLeft())
        {
            botInput.LeftAction = true;
        }
        else if(!RayCollisionCheck(Vector3.right + Vector3.forward) &&
                !RayCollisionCheck(Vector3.right) &&
                playerControl.CanMoveRight())
        {
            botInput.RightAction = true;
        }
        else
        {
            botInput.JumpAction = true;
        }

        AIScanTimer = AIIntevalBetweenActions;
    }

    bool RayCollisionCheck(Vector3 direction)
    {
        RaycastHit hit;
        
        return Physics.Raycast(transform.position + raycastOffset, direction, out hit, maxDistanceCheck) 
            && hit.transform.gameObject.tag == "Damageable";
    }
}