﻿using UnityEngine;
using System;

[Serializable]
public class Road  
{
    [SerializeField]
    private ElementList leftCorridor;
    [SerializeField]
    private ElementList middleCorridor;
    [SerializeField]
    private ElementList rightCorridor;
    [SerializeField]
    private ElementList offroadElements;

    public ElementList LeftCorridor    { get { return leftCorridor;} }
    public ElementList MiddleCorridor  { get { return middleCorridor;} }
    public ElementList RightCorridor   { get { return rightCorridor;} }
    public ElementList OffroadElements { get { return offroadElements; } }
}