﻿using UnityEngine;
using System.Collections;

public class ObjectCleaner : MonoBehaviour 
{
    [SerializeField]
    private float distance;

	void FixedUpdate () 
	{
        transform.SetPositionZ(GameInfoManager.Instance.LastPlayer.z - distance);
    }

    void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
    }
}
