﻿using System.Collections.Generic;

public class PathInfo
{
    private const CellTraversableState defaultTraversable = CellTraversableState.Walkable;
    private const AltitudeLevel        defaultAltitude    = AltitudeLevel.First;

    public Grid<CellTraversableState> Traversable       { set; get; }
    public Grid<AltitudeLevel>        Altitude          { set; get; }
    public Grid<Element>              RoadElements      { set; get; }
    public Grid<Element>              SurrElements      { set; get; }
    public Grid<Element>              GroundObjects     { set; get; }
    public Grid<Element>              PassableObstacles { set; get; }
    public Grid<Element>              Collectable       { set; get; }

    private PathInfo() {}

    public int Length
    {
        get
        {
            if(Traversable == null)
            {
                return 0;
            }

            return GroundObjects.CountRows();
        }
    }

    public List<Grid<Element>> ElementGrids
    {
        get
        {
            return new List<Grid<Element>>()
            {
                RoadElements,
                SurrElements,
                GroundObjects,
                PassableObstacles,
                Collectable
            };
        }
    }

    public class Builder
    {
        PathInfo path;

        public Builder()
        {
            path = new PathInfo();
        }

        public Builder AddTraversable(Grid<CellTraversableState> traversable)
        {
            path.Traversable = traversable;

            return this;
        }

        public Builder AddAltitude(Grid<AltitudeLevel> altitude)
        {
            path.Altitude = altitude;

            return this;
        }

        public Builder AddRoadElement(Grid<Element> elements)
        {
            path.RoadElements = elements;

            return this;
        }

        public Builder AddSurrElement(Grid<Element> elements)
        {
            path.SurrElements = elements;

            return this;
        }

        public Builder AddGroundObject(Grid<Element> elements)
        {
            path.GroundObjects = elements;

            return this;
        }

        public Builder AddPassableObstacles(Grid<Element> elements)
        {
            path.PassableObstacles = elements;

            return this;
        }

        public Builder AddCollectable(Grid<Element> elements)
        {
            path.Collectable = elements;

            return this;
        }

        public PathInfo Build()
        {
            return path;
        }

        public Builder SetDefaultWithSize(int sizeX, int sizeY)
        {
            path.Traversable       = new Grid<CellTraversableState>(sizeX, sizeY, defaultTraversable);
            path.Altitude          = new Grid<AltitudeLevel>(sizeX, sizeY, defaultAltitude);
            path.RoadElements      = new Grid<Element>(sizeX, sizeY);
            path.SurrElements      = new Grid<Element>(sizeX, sizeY);
            path.GroundObjects     = new Grid<Element>(sizeX, sizeY);
            path.PassableObstacles = new Grid<Element>(sizeX, sizeY);
            path.Collectable       = new Grid<Element>(sizeX, sizeY);
            
            return this;
        }
    }

    public void CleanElementGrids()
    {
        RoadElements      = new Grid<Element>();
        SurrElements      = new Grid<Element>();
        GroundObjects     = new Grid<Element>();
        PassableObstacles = new Grid<Element>();
        Collectable       = new Grid<Element>();
    }

    public void Concat(PathInfo otherPath)
    {
        Traversable.Concat(otherPath.Traversable);
        Altitude.Concat(otherPath.Altitude);
        RoadElements.Concat(otherPath.RoadElements);
        SurrElements.Concat(otherPath.SurrElements);
        GroundObjects.Concat(otherPath.GroundObjects);
        PassableObstacles.Concat(otherPath.PassableObstacles);
        Collectable.Concat(otherPath.Collectable);
    }

    public void Prepend(PathInfo otherPath)
    {
        Traversable.Prepend(otherPath.Traversable);
        Altitude.Prepend(otherPath.Altitude);
        RoadElements.Prepend(otherPath.RoadElements);
        SurrElements.Prepend(otherPath.SurrElements);
        GroundObjects.Prepend(otherPath.GroundObjects);
        PassableObstacles.Prepend(otherPath.PassableObstacles);
        Collectable.Prepend(otherPath.Collectable);
    }
}
