﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ListLengthSearcher<T> where T : Element
{
    private Dictionary<int, List<T>> elements;

    public ListLengthSearcher(List<T> list)
    {
        elements = new Dictionary<int, List<T>>();

        foreach(T value in list)
        {
            int key = value.Length;

            if (!elements.ContainsKey(key))
            {
                elements.Add(key, new List<T>());
            }

            elements[key].Add(value);
        }
    }

    public T RandomValue(int maxLength)
    {
        int randomLength = elements.Keys.Where((int length) => { return length <= maxLength; }).ToList().RandomValue();

        return elements[randomLength].RandomValue();
    }

    public void PrintValues()
    {
        foreach(var element in elements)
        {
            Debug.Log(element.Key + " " + element.Value.Count);
        }
    }
}
