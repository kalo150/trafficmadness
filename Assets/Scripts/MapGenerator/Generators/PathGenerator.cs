﻿using UnityEngine;

public class PathGenerator : MonoBehaviour
{
    [SerializeField]
    private int length;

    public bool GenerateTerminal(int Totalcorridors, PathInfo path, int pathLength)
    {
        SurroundingsPartGenerator.Instance.GeneratePart(pathLength, Totalcorridors, path);
        GroundPartGenerator      .Instance.GeneratePart(pathLength, Totalcorridors, path);

        return true;
    }

    virtual public bool Generate(int length, int walkableCorridors, int Totalcorridors, PathInfo path)
    {
        return false;
    }

    virtual public float Length()
    {
        return length;
    }
}
