﻿public class PathVehiclesGenerator : PathGenerator 
{
    public override bool Generate(int length, int walkableCorridors, int Totalcorridors, PathInfo path)
    {
        SurroundingsPartGenerator.Instance.GeneratePart(length, Totalcorridors   , path);
        GroundPartGenerator      .Instance.GeneratePart(length, Totalcorridors   , path);
        VehiclesRoadPartGenerator.Instance.GeneratePart(length, walkableCorridors, path);
        CollectablePartGenerator .Instance.GeneratePart(length, walkableCorridors, path);

        return true;
    }
}
