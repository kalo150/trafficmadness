﻿using System.Collections.Generic;
    
public enum GeneratorState
{
    Start,
    InUse,
    End
}

public class PathGeneratorManager : Singleton<PathGeneratorManager> 
{
    private PathGenerator currentGenerator;
    private float lastDistanceChanged;

    private GeneratorState currentState;

    public GeneratorState CurrentState { get { return currentState; } }

    private delegate void GeneratorStateHandler(PathInfo path, PathInfo newPathInfo);
    private Dictionary<GeneratorState, GeneratorStateHandler> handlers;

    void Awake()
    {
        InitHandlers();
    }

    void Start()
    {
        SetRandomGenerator();
    }

    void FixedUpdate()
    {
        if(ShouldChangeGenerator())
        {
            currentState = GeneratorState.End;
        }
    }

    void InitHandlers()
    {
        handlers = new Dictionary<GeneratorState, GeneratorStateHandler>()
        {
            { GeneratorState.Start, StartState },
            { GeneratorState.End  , EndState   }
        };
    }

    public void Generate(int length, int walkableCorridors, int totalcorridors, PathInfo path)
    {
        NormalGeneration(length, walkableCorridors, totalcorridors, path);

        if(currentState != GeneratorState.InUse)
        {
            AdditionalGeneration(walkableCorridors, totalcorridors, path);
        }
    }

    void NormalGeneration(int length, int walkableCorridors, int totalcorridors, PathInfo path)
    {
        var oldstate = currentState;
        currentState = GeneratorState.InUse;

        path.CleanElementGrids();
        currentGenerator.Generate(length, walkableCorridors, totalcorridors, path);

        currentState = oldstate;
    }

    void AdditionalGeneration(int walkableCorridors, int totalcorridors, PathInfo path)
    {
        int pathLength = MapPackManager
            .Instance
            .CurrentPack
            .GetRoadList()
            .MiddleCorridor
            .RandomElement()
            .Length;

        var newPathInfo = new PathInfo.Builder().SetDefaultWithSize(walkableCorridors, pathLength).Build();
        currentGenerator.GenerateTerminal(totalcorridors, newPathInfo, pathLength);

        handlers[currentState](path, newPathInfo);
    }

    void StartState(PathInfo path, PathInfo newPathInfo)
    {
        for (int i = 0; i < newPathInfo.Length; i++)
        {
            newPathInfo.SurrElements[i] = path.SurrElements[i];
        }

        path.Prepend(newPathInfo);

        currentState = GeneratorState.InUse;
    }

    void EndState(PathInfo path, PathInfo newPathInfo)
    {
        for(int i = 0; i < newPathInfo.Length; i++)
        {
            newPathInfo.SurrElements[i] = path.SurrElements[i];
        }

        path.Concat(newPathInfo);
        
        SetRandomGenerator();
        MapPackManager.Instance.TryToChangePack();

        currentState = GeneratorState.Start;
    }

    bool ShouldChangeGenerator()
    {
        return (GameInfoManager.Instance.FirstPlayer.z >= currentGenerator.Length() + lastDistanceChanged
            || MapPackManager.Instance.ShouldChangePack()) && currentState == GeneratorState.InUse; 
    }

    void SetRandomGenerator()
    {
        var generators = MapPackManager.Instance.CurrentPack.Generators;

        currentGenerator = generators.RandomWithPriority().Generator;

        lastDistanceChanged = GameInfoManager.Instance.FirstPlayer.z;
    }
}
