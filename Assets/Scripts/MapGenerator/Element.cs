﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public class Element
{
	[SerializeField]
    private GameObject prefab;
    [SerializeField]
    private int length;
    [SerializeField]
    private int width;

    public GameObject Prefab { get { return prefab; } }
    public int        Length { get { return length; } }
    public int        Width  { get { return width;  } }
}
