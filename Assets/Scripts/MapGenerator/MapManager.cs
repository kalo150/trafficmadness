﻿using UnityEngine;

public class MapManager : Singleton<MapManager> 
{
    [SerializeField]
    private int totalCorridors;
    [SerializeField]
    private int walkableCorridors;
    [SerializeField]
    private float forwardObjectSpawnDistance;
    [SerializeField]
    private int objectSpawnedAtOnce;

    public int WalkableCorridors { get { return walkableCorridors; } }
    public int TotalCorridors    { get { return totalCorridors;    } } 

    private float lastObjectPositionZ;
    private PathInfo path;

    void Start()
    {
        path = new PathInfo.Builder().SetDefaultWithSize(walkableCorridors, 1).Build();
        lastObjectPositionZ = 0;
    }

    void FixedUpdate()
    {
        if (ShouldGeneratePath())
        {
            GeneratePathBlocks();
        }
    }

    void GeneratePathBlocks()
    {
        PathGeneratorManager.Instance.Generate(objectSpawnedAtOnce, walkableCorridors, totalCorridors, path);
        SpawnManager.Instance.SpawnPath(path, lastObjectPositionZ);
        lastObjectPositionZ += path.Length * SpawnManager.Instance.CellLength;
    }

    bool ShouldGeneratePath()
    {
        return GameInfoManager.Instance.FirstPlayer.z >= lastObjectPositionZ - forwardObjectSpawnDistance;
    }

    public int StartWalkableIndex()
    {
        return (totalCorridors - walkableCorridors) / 2;
    }

    public int EndWalkableIndex()
    {
        return StartWalkableIndex() + walkableCorridors;
    }

    public bool IsWalkable(int indexX)
    {
        return indexX >= StartWalkableIndex() && indexX < EndWalkableIndex();
    }
}
