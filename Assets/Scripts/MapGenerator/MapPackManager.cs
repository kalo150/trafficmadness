﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MapPackManager : Singleton<MapPackManager> 
{
    [SerializeField]
    private List<ObjectPack> packs;

    private ObjectPack currentPack;
    private float lastDistancePackChanged;
 
    public ObjectPack CurrentPack
    {
        get 
        {
            if(currentPack == null)
            {
                GenerateNewPack();
            }

            return currentPack;
        }
    }

    void Start()
    {
        lastDistancePackChanged = 0;
    }

    public void TryToChangePack()
    {
        if (ShouldChangePack())
        {
            GenerateNewPack();
        }
    }

    public bool ShouldChangePack()
    {
        return GameInfoManager.Instance.FirstPlayer.z >= lastDistancePackChanged + currentPack.Length;
    }

    public void GenerateNewPack()
    {
        currentPack = packs.RandomWithPriority();

        lastDistancePackChanged = GameInfoManager.Instance.FirstPlayer.z;
    }
}
