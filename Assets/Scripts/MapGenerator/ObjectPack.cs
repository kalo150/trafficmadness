﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class ObjectPack : PriorityItem
{
    public string packName;

    [SerializeField]
    private float packLength;

    [SerializeField]
    private List<GeneratorWithPriority> generators;

    [SerializeField]
    private PathElementList pathObjects;
    [SerializeField] 
    private PathElementList obstaclesObjects;
    
    [SerializeField]
    private Road road;
    [SerializeField]
    private Road startPackRoadElements;
    [SerializeField]
    private Road endPackRoadElements;

    [SerializeField]
    private ElementList intermediate;
    [SerializeField]
    private ElementList buildings;
    [SerializeField]
    private ElementList vehicles;

    [SerializeField]
    private List<ComplexElement> passableObstacles;

    public float                       Length            { get { return packLength;        } }
    public List<GeneratorWithPriority> Generators        { get { return generators;        } }
    public PathElementList             PathObjects       { get { return pathObjects;       } }
    public PathElementList             ObstaclesObjects  { get { return obstaclesObjects;  } }
    public ElementList                 Intermediate      { get { return intermediate;      } }
    public ElementList                 Buildings         { get { return buildings;         } }
    public ElementList                 Vehicles          { get { return vehicles;          } }
    public List<ComplexElement>        PassableObstacles { get { return passableObstacles; } }

    public Road GetRoadList()
    {
        switch (PathGeneratorManager.Instance.CurrentState)
        {
            case GeneratorState.Start:
                return startPackRoadElements;
            case GeneratorState.End:
                return endPackRoadElements;
            default:
                return road;
        }
    }
}

