﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GeneratorWithPriority : PriorityItem
{
    [SerializeField]
	private PathGenerator generator;

    public PathGenerator Generator { get { return generator; } }
}
