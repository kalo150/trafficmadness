﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class CollectableWithPriority : PriorityItem 
{
    [SerializeField]
    private Element element;

    public Element Element { get { return element; } }
}
