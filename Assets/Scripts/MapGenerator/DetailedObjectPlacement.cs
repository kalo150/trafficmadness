﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class DetailedObjectPlacement
{
    [SerializeField]
    private int numberCorridor;
    [SerializeField]
    private Vector3 rotation;

    public int     NumberCorridor { get { return numberCorridor; } }
    public Vector3 Rotation       { get { return rotation;       } }     
    
    public static DetailedObjectPlacement identity
    {
        get
        {
            return new DetailedObjectPlacement();
        }
    }

    public DetailedObjectPlacement()
    {
        rotation = new Vector3();
    }
}

