﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

[Serializable]
public class ElementList
{
    [SerializeField]
    private List<Element> elements;

    private ListLengthSearcher<Element> elementSearcher;
    private ListLengthSearcher<Element> ElementSearcher
    {
        get
        {
            if (elementSearcher == null)
            {
                elementSearcher = new ListLengthSearcher<Element>(elements);
            }

            return elementSearcher;
        }
    }

    public Element RandomElement()
    {
        return elements.RandomValue();
    }

    public Element RandomElement(int maxLength)
    {
        return ElementSearcher.RandomValue(maxLength);
    }

    public Element RandomElementMaxLength(int maxLength)
    {
        var list = elements.Where(x => x.Length <= maxLength).ToList();
        
        return list.Count > 0 ? list.RandomValue() : null;
    }
}
