﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class SpawnManager : Singleton<SpawnManager> 
{
    [SerializeField]
    private float cellWidth;
    [SerializeField]
    private float cellLength;
    [SerializeField]
    private List<DetailedObjectPlacement> objectPlacement;

    public float CellWidth  { get { return cellWidth;  } }
    public float CellLength { get { return cellLength; } }

    public void SpawnPath(PathInfo path, float startPosition)
    {
        foreach(var elementGrid in path.ElementGrids)
        {
            if(elementGrid != null)
            {
                SpawnObjects(elementGrid, new Vector3(0f, 0f, startPosition));
            }
        }
    }

    public void SpawnObjects<T>(Grid<T> grid, Vector3 startPosition) where T : Element
    {
        for (int x = 0; x < grid.CountCols(); x++)
        {
            var rotations = GetCorridorRotation(x);
            Element currentElement;
            for (int y = 0; y < grid.CountRows();)
            {
                currentElement = grid[y][x];
                
                if(currentElement == null)
                {
                    y++;

                    continue;
                }
                
                var elementPrefab = currentElement.Prefab;

                if (elementPrefab != null)
                {
                    var elementPosition = IndexToPosition(x, y, currentElement.Length, grid.CountCols(), startPosition, elementPrefab);

                    var newRotation = Quaternion.Euler(rotations.RandomValue().Rotation) * elementPrefab.transform.rotation;
                    Instantiate(elementPrefab, elementPosition, newRotation);
                }

                y += currentElement.Length;
            }
        }
    }

    public void SpawnObject(GameObject spawnObject, Vector3 position = default(Vector3))
    {
        if (spawnObject != null)
        {
            Instantiate(spawnObject, position + spawnObject.transform.position, Quaternion.identity);
        }
    }

    Vector3 IndexToPosition(int x, int y, int length, int numberCorridors, Vector3 startPosition, GameObject prefab)
    {
        var prefabPosition = prefab.transform.position;

        float xPosition = x * cellWidth - ((numberCorridors * cellWidth - cellWidth) / 2f);
        float yPosition = 0f;
        float zPosition = (y * cellLength - (length / 2f));

        return new Vector3(xPosition, yPosition, zPosition) + startPosition + prefabPosition;
    }

    List<DetailedObjectPlacement> GetCorridorRotation(int corridorIndex)
    {
        var placementObjects = objectPlacement.Where(x => x.NumberCorridor == corridorIndex).ToList();

        if(placementObjects.Count == 0)
        {
            placementObjects.Add(DetailedObjectPlacement.identity);
        }

        return placementObjects;
    }
}