﻿public class VehiclesRoadPartGenerator : PartGenerator<VehiclesRoadPartGenerator> 
{
    private TraversableCellsGenerator      traversableCellsGenerator;
    private AltitudeLevelGenerator         altitudeLevelGenerator;
    private MovingVehiclesGeneratorObjects movingVehiclesGeneratorObjects;

    void Awake()
    {
        traversableCellsGenerator       = GetComponent<TraversableCellsGenerator>();
        altitudeLevelGenerator          = GetComponent<AltitudeLevelGenerator>();
        movingVehiclesGeneratorObjects  = GetComponent<MovingVehiclesGeneratorObjects>();
    }

    public override bool GeneratePart(int length, int numberCorridors, PathInfo path)
    {
        var previousTraversableLine = path.Traversable.GetLastRow().Clone();
        var previousAltitudeLine    = path.Altitude.GetLastRow().Clone();

        traversableCellsGenerator.Generate(path, length, numberCorridors, previousTraversableLine);
        altitudeLevelGenerator.Generate(path, previousAltitudeLine, previousTraversableLine);
        movingVehiclesGeneratorObjects.Generate(path, previousAltitudeLine, previousTraversableLine);

        return true;
    }
}
