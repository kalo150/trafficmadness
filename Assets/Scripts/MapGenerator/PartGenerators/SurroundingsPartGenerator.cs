﻿using UnityEngine;
using System.Collections.Generic;

public class SurroundingsPartGenerator : PartGenerator<SurroundingsPartGenerator>
{
    [SerializeField][Range(0, 100)]
    private float chanceToSpawnIntermediate;
    [SerializeField][Range(0, 100)]
    private float chanceToSpawnBuildings;
    [SerializeField]
    private List<int> intermediateCorridors;
    [SerializeField]
    private List<int> buildingsCorridors;

    public override bool GeneratePart(int length, int totalCorridors, PathInfo path)
    {
        var intermediate = MapPackManager.Instance.CurrentPack.Intermediate;
        var buildings    = MapPackManager.Instance.CurrentPack.Buildings;

        var grid = new Grid<Element>(totalCorridors, length, null);

        foreach (int indexCorridor in buildingsCorridors)
        {
            GenerateObjects(length, indexCorridor - 1, buildings, chanceToSpawnBuildings, ref grid);
        }

        foreach (int indexCorridor in intermediateCorridors)
        {
            GenerateObjects(length, indexCorridor - 1, intermediate, chanceToSpawnIntermediate, ref grid);
        }
        
        path.SurrElements = grid;

        return true;
    }
    
    void GenerateObjects(int length, int corridorIndex, ElementList objects, float spawnPercentage, ref Grid<Element> grid)
    {
        int currentLength = length;
        while (currentLength > 0)
        {
            if (GameUtils.MathUtils.RandomWithPercentage(spawnPercentage))
            {
                Element randomElement = objects.RandomElement(currentLength);
                grid[length - currentLength][corridorIndex] = randomElement;
                currentLength -= randomElement.Length;
            }
            else
            {
                currentLength--;
            }
        }
    }
}
