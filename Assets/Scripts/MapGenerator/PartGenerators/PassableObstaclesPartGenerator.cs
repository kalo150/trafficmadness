﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class PassableObstaclesPartGenerator : PartGenerator<PassableObstaclesPartGenerator> 
{
    [SerializeField][Range(0, 100)]
    private float spawnFrequency;
    [SerializeField]
    private int distanceClosestObject;
    [SerializeField]
    private List<AltitudeLevel> altitudeLevels;

    public override bool GeneratePart(int length, int totalCorridors, PathInfo path)
    {
        var passableObstacles = MapPackManager.Instance.CurrentPack.PassableObstacles;

        var objects = new Grid<Element>(totalCorridors, length);

        for(int y = 0; y < length; y++)
        {
            for(int x = 0; x < totalCorridors; x++)
            {
                if(ValidPassableObjectCell(x, y, path) && GameUtils.MathUtils.RandomWithPercentage(spawnFrequency))
                {
                    objects[y][x] = GenerateObject(passableObstacles, path.Altitude[y][x]);
                    y += distanceClosestObject;
                }
            }
        }

        path.PassableObstacles = objects;

        return true;
    }

    private Element GenerateObject(List<ComplexElement> passableObstacles, AltitudeLevel altitude)
    {
        var elements = passableObstacles.Where(elementArg => elementArg.StartAltitudeLevel == altitude).ToList();

        return elements.RandomValue();
    }

    bool ValidPassableObjectCell(int currentX, int currentY, PathInfo path)
    {
        if(currentY - distanceClosestObject < 0 || currentY + distanceClosestObject >= path.Altitude.CountRows())
        {
            return false;
        }

        for (int y = currentY - distanceClosestObject; y < currentY + distanceClosestObject; y++)
        {
            if (!altitudeLevels.Contains(path.Altitude[y][currentX]) || 
                path.Traversable[y][currentX] != CellTraversableState.Walkable)
            {
                return false;
            }
        }

        return true;
    }
}
