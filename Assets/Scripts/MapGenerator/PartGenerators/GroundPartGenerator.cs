﻿public class GroundPartGenerator : PartGenerator<GroundPartGenerator> 
{
    ElementList rightElements;
    ElementList leftElements;
    ElementList middleElements;
    ElementList offroadElements;

    public override bool GeneratePart(int length, int totalCorridors, PathInfo path)
    {
        var road = MapPackManager.Instance.CurrentPack.GetRoadList();

        rightElements   = road.RightCorridor;
        leftElements    = road.LeftCorridor;
        middleElements  = road.MiddleCorridor;
        offroadElements = road.OffroadElements;

        Grid<Element> groundObjects = new Grid<Element>(totalCorridors, length);

        for (int currentCorridor = 0; currentCorridor < totalCorridors; currentCorridor++)
        {
            var roadObject = GetRoadElements(currentCorridor);

            int currentLength = length;
            while (currentLength > 0)
            {
                var distanceIndex = length - currentLength;

                groundObjects[distanceIndex][currentCorridor] = roadObject.RandomElement(currentLength);

                currentLength -= groundObjects[distanceIndex][currentCorridor].Length;
            }
        }

        path.GroundObjects = groundObjects;

        return true;
    }

    ElementList GetRoadElements(int indexCorridor)
    {
        ElementList roadElements = middleElements;

        if (IsOffroadCorridor(indexCorridor))
        {
            roadElements = offroadElements;
        }
        else if (IsLeftRoadEnd(indexCorridor))
        {
            roadElements = leftElements;
        }
        else if (IsRightRoadEnd(indexCorridor))
        {
            roadElements = rightElements;
        }

        return roadElements;
    }

    bool IsRightRoadEnd(int indexCorridor)
    {
        return !MapManager.Instance.IsWalkable(indexCorridor + 1);
    }

    bool IsLeftRoadEnd(int indexCorridor)
    {
        return !MapManager.Instance.IsWalkable(indexCorridor - 1);
    }

    bool IsOffroadCorridor(int indexCorridor)
    {
        return !MapManager.Instance.IsWalkable(indexCorridor);
    }
}
