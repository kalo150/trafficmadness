﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;


public class CollectablePartGenerator : PartGenerator<CollectablePartGenerator> 
{
    public bool CrazyModActive { get; set; }

    public enum CoinPackType
    {
        Straight,
        Left,
        Right,
        Parabola
    }

    [System.Serializable]
    private class KeyValueCoinPack
    {
        public CoinPackType type;
        public Element pack;
    };

    [SerializeField]
    private List<CollectableWithPriority> powerUps;
    [SerializeField][Range(0, 100)]
    private float powerUpSpawnChance;
    [SerializeField]
    private List<KeyValueCoinPack> coinPacksList;
    [SerializeField]
    private List<KeyValueCoinPack> noGravityCoinPacksList;
    [SerializeField]
    private Element crazyCoinPack;
    [SerializeField][Range(0, 100)]
    private float coinSpawnFrequency;
    [SerializeField][Range(0, 100)]
    private float chanceParabolaCoinPack;
    [SerializeField][Range(0, 100)]
    private float chanceCoinChangeCorridor;
    [SerializeField]
    private int minCoinsSpawnedAtOnce;
    [SerializeField]
    private int maxCoinsSpawnedAtOnce;

    private Dictionary<CoinPackType, Element> coinPacks;
    private Dictionary<CoinPackType, Element> noGravityCoinPacks;

    public Dictionary<CoinPackType, Element> NoGravityCoinPacks
    {
        get { return noGravityCoinPacks; }
    }

    void Awake()
    {
        coinPacks = coinPacksList.ToDictionary(t => t.type, t => t.pack);
        noGravityCoinPacks = noGravityCoinPacksList.ToDictionary(t => t.type, t => t.pack);
    }

    public override bool GeneratePart(int length, int totalCorridors, PathInfo path)
    {
        var collectableGrid = new Grid<Element>(totalCorridors, length);

        for(int x = 0; x < totalCorridors; x++)
        {
            for(int y = 0; y < length; y++)
            {
                if(!IsCellWalkable(x, y, path))
                {
                    continue;
                }

                if (GameUtils.MathUtils.RandomWithPercentage(powerUpSpawnChance))
                {
                    AddPowerUp(x, ref y, collectableGrid);
                }
                else if (GameUtils.MathUtils.RandomWithPercentage(coinSpawnFrequency))
                {
                    AddCoin(x, ref y, collectableGrid, path);
                }
            }
        }
        
        path.Collectable = collectableGrid;

        return true;
    }

    void AddPowerUp(int x, ref int y, Grid<Element> grid)
    {
        grid[y][x] = powerUps.RandomWithPriority().Element;
    }

    void AddCoin(int x, ref int y, Grid<Element> grid, PathInfo path)
    {
        int numberCoins = Random.Range(minCoinsSpawnedAtOnce, maxCoinsSpawnedAtOnce);

        for(int i = 0; i < numberCoins && y < grid.CountRows(); i++)
        {
            if(CrazyModActive)
            {
                grid[y][x] = crazyCoinPack;
            }
            else if (ShouldContinueCorridor(x, y, path))
            {
                SameCorridorGeneration(x, y, grid);
            }
            else if (IsCellWalkable(x, y - 1, path))
            {
                TryNewCorridorGeneration(ref x, y, grid, path);
            }

            y += grid[y][x] != null ? grid[y][x].Length : 1;
        }
    }

    void TryNewCorridorGeneration(ref int x, int y, Grid<Element> grid, PathInfo path)
    {
        if (IsCellWalkable(x - 1, y, path) && GameUtils.MathUtils.RandomWithPercentage(50f))
        {
            grid[y][x--] = coinPacks[CoinPackType.Left];
        }
        else if (IsCellWalkable(x + 1, y, path))
        {
            grid[y][x++] = coinPacks[CoinPackType.Right];
        }
    }

    void SameCorridorGeneration(int x, int y, Grid<Element> grid)
    {
        if (GameUtils.MathUtils.RandomWithPercentage(chanceParabolaCoinPack))
        {
            grid[y][x] = coinPacks[CoinPackType.Parabola];
        }
        else
        {
            grid[y][x] = coinPacks[CoinPackType.Straight];
        }
    }

    bool ShouldContinueCorridor(int x, int y, PathInfo path)
    {
        bool canChange = GameUtils.MathUtils.RandomWithPercentage(chanceCoinChangeCorridor);

        return IsCellWalkable(x, y, path) && !canChange;
    }

    bool IsCellWalkable(int x, int y, PathInfo path)
    {
        if(!path.Traversable.ValidIndex(x, y))
        {
            return false;
        }

        bool traversable = path.Traversable[y][x] == CellTraversableState.Walkable;
        bool passable = !path.PassableObstacles.ValidIndex(x, y) || path.PassableObstacles[y][x] == null;

        return traversable && passable;
    }
}
