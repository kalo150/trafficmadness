﻿public class ObstaclesPartGenerator : PartGenerator<ObstaclesPartGenerator>
{
    private TraversableCellsGenerator traversableCellsGenerator;
    private AltitudeLevelGenerator    altitudeLevelGenerator;
    private ObjectPositionsGenerator  objectPositionsGenerator;

    void Awake()
    {
        traversableCellsGenerator = GetComponent<TraversableCellsGenerator>();
        altitudeLevelGenerator    = GetComponent<AltitudeLevelGenerator>();
        objectPositionsGenerator  = GetComponent<ObjectPositionsGenerator>();
    }

    public override bool GeneratePart(int length, int numberCorridors, PathInfo path)
    {
        var previousTraversableLine = path.Traversable.GetLastRow().Clone();
        var previousAltitudeLine    = path.Altitude.GetLastRow().Clone();

        traversableCellsGenerator.Generate(path, length, numberCorridors, previousTraversableLine);
        altitudeLevelGenerator.Generate(path, previousAltitudeLine, previousTraversableLine);
        objectPositionsGenerator.Generate(path, previousAltitudeLine, previousTraversableLine);

        return true;
    }
}
