﻿public abstract class PartGenerator<T> : Singleton<T> where T : Singleton<T>
{
    public abstract bool GeneratePart(int length, int totalCorridors, PathInfo path);
}
