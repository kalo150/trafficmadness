﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPositionsGenerator : MonoBehaviour
{
    private FastAltitudeSearcher pathAltitude;
    private FastAltitudeSearcher obstacleAltitude;

    public bool Generate(PathInfo path, List<AltitudeLevel> previousAltitudeLine, List<CellTraversableState> previousTraversableLine)
    {
        pathAltitude     = MapPackManager.Instance.CurrentPack.PathObjects.Searcher;
        obstacleAltitude = MapPackManager.Instance.CurrentPack.ObstaclesObjects.Searcher;

        Grid<Element> mapElements = new Grid<Element>(path.Altitude.CountCols(), path.Altitude.CountRows());

        for (int x = 0; x < path.Altitude.CountCols(); x++)
        {
            var currentCellState    = path.Traversable[0][x];
            var currentAltitude     = path.Altitude[0][x];
            var previousAltitude    = previousAltitudeLine[x];
            var previousTraversable = previousTraversableLine[x];

            int lineLength = 1;
            for (int y = 1; y <= path.Altitude.CountRows(); y++, lineLength++)
            {
                if (NewObjectRelation(x, y, currentCellState, currentAltitude, path))
                {
                    List<Element> objects;
                    if (currentCellState == CellTraversableState.Obstacle)
                    {
                        objects = GenerateLine(lineLength, currentAltitude, currentAltitude, obstacleAltitude);
                    }
                    else if (previousTraversable == CellTraversableState.Obstacle)
                    {
                        objects = GenerateLine(lineLength, currentAltitude, currentAltitude, pathAltitude);
                    }
                    else
                    {
                        objects = GenerateLine(lineLength, currentAltitude, previousAltitude, pathAltitude);
                    }

                    for (int objectIndex = 0, gridIndex = y - lineLength; objectIndex < objects.Count; objectIndex++)
                    {
                        for (int i = 0; i < objects[objectIndex].Length; i++)
                        {
                            mapElements[gridIndex++][x] = objects[objectIndex];
                        }
                    }

                    if (path.Altitude.ValidIndex(x, y))
                    {
                        previousTraversable = currentCellState;
                        currentCellState    = path.Traversable[y][x];
                        previousAltitude    = currentAltitude;
                        currentAltitude     = path.Altitude[y][x];
                        lineLength          = 0;
                    }
                }
            }
        }

        path.RoadElements = mapElements;

        return true;
    }

    private bool NewObjectRelation(int x, int y, CellTraversableState currentCellState, AltitudeLevel currentAltitude, PathInfo path)
    {
        return y >= path.Altitude.CountRows() 
            || currentAltitude != path.Altitude[y][x] 
            || currentCellState != path.Traversable[y][x];
    }

    List<Element> GenerateLine(int length, AltitudeLevel currentAltitude, AltitudeLevel previousAltitude, FastAltitudeSearcher altitude)
    {
        var corridorObjects = new List<Element>();
        AltitudeLevel startAltitude = previousAltitude;

        while(length > 0)
        {
            var mapObject = altitude.GetRandomObject(startAltitude, currentAltitude, length);

            if(mapObject == null)
            {
                break;
            }

            length -= mapObject.Length;
            corridorObjects.Add(mapObject);

            startAltitude = currentAltitude;
        }

        return corridorObjects;
    }
}
