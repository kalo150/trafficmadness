﻿using UnityEngine;
using System.Collections.Generic;

public class MovingVehiclesGeneratorObjects : MonoBehaviour 
{
    private ElementList vehicles;

    public bool Generate(PathInfo path, List<AltitudeLevel> previousAltitudeLine, List<CellTraversableState> previousTraversableLine)
    {
        vehicles = MapPackManager.Instance.CurrentPack.Vehicles;

        var mapElements = new Grid<Element>(path.Altitude.CountCols(), path.Altitude.CountRows());

        for (int x = 0; x < path.Altitude.CountCols(); x++)
        {
            var currentCellState = path.Traversable[0][x];

            int lineLength = 1;
            for (int y = 1; y <= path.Altitude.CountRows(); y++, lineLength++)
            {
                if (NewObjectRelation(x, y, currentCellState, path))
                {
                    if(currentCellState == CellTraversableState.Obstacle)
                    {
                        var objects = GenerateLine(lineLength);

                        for (int objectIndex = 0, gridIndex = y - lineLength; objectIndex < objects.Count; objectIndex++)
                        {
                            for (int i = 0; i < objects[objectIndex].Length; i++)
                            {
                                mapElements[gridIndex++][x] = objects[objectIndex];
                            }
                        }
                    }

                    if (path.Altitude.ValidIndex(x, y))
                    {
                        currentCellState = path.Traversable[y][x];
                        lineLength = 0;
                    }
                }
            }
        }

        path.RoadElements = mapElements;

        return true;
    }

    private bool NewObjectRelation(int x, int y, CellTraversableState currentCellState, PathInfo path)
    {
        return y >= path.Altitude.CountRows() || currentCellState != path.Traversable[y][x];
    }

    List<Element> GenerateLine(int length)
    {
        var corridorObjects = new List<Element>();

        while (length > 0)
        {
            var mapObject = vehicles.RandomElementMaxLength(length);

            if(mapObject == null)
            {
                break;
            }

            length -= mapObject.Length;
            corridorObjects.Add(mapObject);
        }

        return corridorObjects;
    }
}
