﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum AltitudeLevel
{
    Undefined = 0,
    First,
    Second,

    TotalLevels
}

public enum CellTraversableState
{
    Obstacle = 0,
    Walkable,

    NumStates
}

[System.Serializable]
public class ComplexElement : Element
{
    [SerializeField]
    private AltitudeLevel startAltitudeLevel;
    [SerializeField]
    private AltitudeLevel endAltitudeLevel;

    public AltitudeLevel StartAltitudeLevel { get { return startAltitudeLevel; } }
    public AltitudeLevel EndAltitudeLevel   { get { return endAltitudeLevel;   } }
}