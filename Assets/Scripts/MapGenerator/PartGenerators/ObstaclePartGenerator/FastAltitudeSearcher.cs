﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class FastAltitudeSearcher
{
    // value[startAltitude][endAltitude] = list of map elements
    private Dictionary<AltitudeLevel, Dictionary<AltitudeLevel, List<ComplexElement>>> fastAltitudeSearch;

    public FastAltitudeSearcher(List<ComplexElement> mapObjects)
    {
        fastAltitudeSearch = new Dictionary<AltitudeLevel, Dictionary<AltitudeLevel, List<ComplexElement>>>();

        foreach (var mapObject in mapObjects)
        {
            if (!fastAltitudeSearch.ContainsKey(mapObject.StartAltitudeLevel))
            {
                fastAltitudeSearch.Add(mapObject.StartAltitudeLevel, new Dictionary<AltitudeLevel, List<ComplexElement>>());
            }

            if(!fastAltitudeSearch[mapObject.StartAltitudeLevel].ContainsKey(mapObject.EndAltitudeLevel))
            {
                fastAltitudeSearch[mapObject.StartAltitudeLevel].Add(mapObject.EndAltitudeLevel, new List<ComplexElement>());
            }

            fastAltitudeSearch[mapObject.StartAltitudeLevel][mapObject.EndAltitudeLevel].Add(mapObject);
        }
    }

    public ComplexElement GetRandomObject(AltitudeLevel start, AltitudeLevel end, int maxLength)
    {
        if (!fastAltitudeSearch.ContainsKey(start) || !fastAltitudeSearch[start].ContainsKey(end))
        {
            return null;
        }

        var possibleObjects = fastAltitudeSearch[start][end].Where(element => element.Length <= maxLength).ToList();

        if (possibleObjects == null || possibleObjects.Count == 0)
        {
            return null;
        }

        return possibleObjects.RandomValue();
    }
}
