﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class PathElementList
{
    [SerializeField]
    private List<ComplexElement> elements;

    private FastAltitudeSearcher searcher;

    public FastAltitudeSearcher Searcher
    {
        get
        {
            if(searcher == null)
            {
                searcher = new FastAltitudeSearcher(elements);
            }

            return searcher;
        }
    }
}
