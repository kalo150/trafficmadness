﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AltitudeLevelGenerator : MonoBehaviour
{
    [SerializeField][Range(0, 100)]
    private float altitudeChangeFrequency;
    [SerializeField][Range(0, 100)]
    private float altitudeChangeUpChance;
    [SerializeField][Range(0, 100)]
    private float obstacleAltitudeChangeFrequency;
    [SerializeField][Range(0, 100)]
    private float obstacleAltitudeChangeUpChance;

    enum Directions
    {
        Down = -1,
        None = 0,
        Up = 1
    }

    public bool Generate(PathInfo path, List<AltitudeLevel> previousLine, List<CellTraversableState> previousTraversableLine)
    {
        var pathGrid     = path.Traversable;
        var altitudeGrid = new Grid<AltitudeLevel>(pathGrid.CountCols(), pathGrid.CountRows());

        altitudeGrid[0] = previousLine.Clone();
        previousLine    = altitudeGrid[0];

        for(int x = 0; x < pathGrid.CountCols(); x++)
        {
            if(previousTraversableLine[x] != CellTraversableState.Walkable)
            {
                altitudeGrid[0][x] = RandomValidNeighbor(altitudeGrid[0], pathGrid[0], x);
            }
        }

        for (int y = 1; y < pathGrid.CountRows(); y++)
        {
            for (int x = 0; x < pathGrid.CountCols(); x++)
            {
                if (pathGrid[y][x] != CellTraversableState.Walkable)
                {
                    if (previousTraversableLine[x] == CellTraversableState.Walkable)
                    {
                        previousLine[x] = RandomValidNeighbor(previousLine, pathGrid[y], x);
                    }
                } 
                else if (ShouldChangeAltitude(previousTraversableLine[x]))
                {
                    altitudeGrid[y][x] = RandomValidAltitudePercentage(previousLine[x]);
                }
                else if (previousTraversableLine[x] == CellTraversableState.Walkable)
                {
                    altitudeGrid[y][x] = previousLine[x];
                }
                else
                {
                    altitudeGrid[y][x] = AltitudeLevel.Undefined;
                }
            }

            for (int x = 0; x < pathGrid.CountCols(); x++)
            {
                if (altitudeGrid[y][x] == AltitudeLevel.Undefined && pathGrid[y][x] == CellTraversableState.Walkable)
                {
                    altitudeGrid[y][x] = RandomValidNeighbor(altitudeGrid[y], pathGrid[y], x);
                }
            }

            for (int x = 0; x < pathGrid.CountCols(); x++)
            {
                if (pathGrid[y][x] != CellTraversableState.Walkable)
                {
                    altitudeGrid[y][x] = RandomAltitude();
                }
            }

            previousLine = altitudeGrid[y];
            previousTraversableLine = pathGrid[y];
        }

        path.Altitude = altitudeGrid;

        return true;
    }

    AltitudeLevel RandomValidAltitudePercentage(AltitudeLevel altitude)
    {
        if (GameUtils.MathUtils.RandomWithPercentage(altitudeChangeUpChance))
        {
            return TryGetHigherLevel(altitude);
        }

        return TryGetLowerLevel(altitude);
    }

    AltitudeLevel RandomValidNeighbor(List<AltitudeLevel> line, List<CellTraversableState> traversable, int index)
    {
        int randomDirection = GameUtils.MathUtils.RandomWithPercentage(50) ? 1 : -1;
        int newIndex = index + randomDirection;
        
        if(!line.ValidIndex(newIndex) || line[newIndex] == AltitudeLevel.Undefined || traversable[newIndex] != CellTraversableState.Walkable)
        {
            newIndex = index - randomDirection;
        }

        if (!line.ValidIndex(newIndex) || line[newIndex] == AltitudeLevel.Undefined || traversable[newIndex] != CellTraversableState.Walkable)
        {
            return AltitudeLevel.First;
        }

        return line[newIndex];
    }

    AltitudeLevel TryGetLowerLevel(AltitudeLevel currentLevel)
    {
        if(HasLowerAltitudeLevel(currentLevel))
        {
            return currentLevel + (int) Directions.Down;
        }

        return currentLevel;
    }

    AltitudeLevel TryGetHigherLevel(AltitudeLevel currentLevel)
    {
        if (HasHigherAltituteLevel(currentLevel))
        {
            return currentLevel + (int) Directions.Up;
        }

        return currentLevel;
    }

    AltitudeLevel RandomValidAltitude(AltitudeLevel previousAltitude)
    {
        float levelChange;

        if (!HasLowerAltitudeLevel(previousAltitude))
        {
            levelChange = (float) Directions.Up;
        }
        else if (!HasHigherAltituteLevel(previousAltitude))
        {
            levelChange = (float) Directions.Down;
        }
        else
        {
            levelChange = GameUtils.MathUtils.RandomWithPercentage(50) ? 1 : -1;
        }

        var newAltitude = (AltitudeLevel) ((float) previousAltitude + levelChange);

        return newAltitude;
    }

    bool HasLowerAltitudeLevel(AltitudeLevel altitudeLevel)
    {
        return altitudeLevel > AltitudeLevel.First;
    }

    bool HasHigherAltituteLevel(AltitudeLevel altitudeLevel)
    {
        return altitudeLevel < AltitudeLevel.TotalLevels - 1;
    }

    bool ShouldChangeAltitude(CellTraversableState state)
    {
        bool changeResult = GameUtils.MathUtils.RandomWithPercentage(altitudeChangeFrequency);
        bool validState   = state == CellTraversableState.Walkable;

        return changeResult && validState;
    }

    public static AltitudeLevel RandomAltitude()
    {
        int first = (int) AltitudeLevel.First;
        int last  = (int) AltitudeLevel.TotalLevels;

        return (AltitudeLevel) Random.Range(first, last);
    }

}
