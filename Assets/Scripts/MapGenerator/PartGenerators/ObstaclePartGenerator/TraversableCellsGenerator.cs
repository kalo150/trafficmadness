﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TraversableCellsGenerator : MonoBehaviour
{
    [SerializeField][Range(0, 100)]
    private float chanceToSplitPath;
    [SerializeField][Range(0, 100)]
    private float pathComplexity;

    public bool Generate(PathInfo path, int gridLength, int gridWidth, List<CellTraversableState> previousLine)
    {
        var traversablePath = new Grid<CellTraversableState>(gridWidth, gridLength);

        traversablePath[0] = previousLine.Clone();
        previousLine = traversablePath[0];

        for (int y = 1; y < gridLength; y++)
        {
            for (int x = 0; x < gridWidth; x++)
            {
                if (previousLine[x] != CellTraversableState.Walkable)
                {
                    continue;
                }

                int minIndex = TryGetLeftCorridor(x);
                int maxIndex = TryGetRightCorridor(x, gridWidth);

                int numPointsToGenerate = RandomNumberPoints(gridWidth);

                for (int generatedPoints = 0; generatedPoints < numPointsToGenerate; generatedPoints++)
                {
                    int randomIndex = GetNextIndex(minIndex, maxIndex);

                    traversablePath[y][randomIndex] = CellTraversableState.Walkable;
                }
            }

            for (int x = 0; x < gridWidth; x++)
            {
                if (traversablePath[y][x] == CellTraversableState.Walkable)
                {
                    previousLine[x] = CellTraversableState.Walkable;
                }
            }

            previousLine = traversablePath[y];
        }

        path.Traversable = traversablePath;

        return true;
    }

    private int GetNextIndex(int startIndex, int endIndex)
    {
        if(GameUtils.MathUtils.RandomWithPercentage(pathComplexity))
        {
            return GameUtils.MathUtils.RandomWithPercentage(50) ? startIndex : endIndex;
        }

        return Random.Range(startIndex, endIndex + 1);
    }

    int RandomNumberPoints(int topBoundIndex)
    {
        int numberPoints = 1;

        if (GameUtils.MathUtils.RandomWithPercentage(chanceToSplitPath))
        {
            numberPoints = Random.Range(2, topBoundIndex + 1);
        }

        return numberPoints;
    }

    int TryGetLeftCorridor(int indexCorridor)
    {
        int newIndex = indexCorridor - 1;

        return newIndex >= 0 ? newIndex : indexCorridor;
    }

    int TryGetRightCorridor(int indexCorridor, int gridWidth)
    {
        int newIndex = indexCorridor + 1;

        return newIndex < gridWidth ? newIndex : indexCorridor;
    }
}
