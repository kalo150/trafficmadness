﻿using UnityEngine;

public class PlayerControl : MonoBehaviour 
{
    private const string ChangeCorridorSpeed = "ChangeLane";

    [SerializeField]
    private float jumpForce;
    [SerializeField]
    private float forwardMovementSpeed;
    [SerializeField]
    private float forwardMovementMaxSpeed;
    [SerializeField]
    private float sidewayMovementSpeed;
    [SerializeField]
    private int currentPlayerCorridor;
    [SerializeField]
    private float accelerationIntervalSpeed;
    [SerializeField]
    private float timeAccelerationInterval;
    [SerializeField]
    private float jumpOnceEverySeconds;

    private PlayerHealth playerHealth;
    private PlayerInput playerInput;
    private float currentTimeAcceleration;
    private float sidewayMovementOffset;
    private bool playerMidair;
    private bool lockedYAxis;
    private float distToGround;
    private Rigidbody playerRB;
    private Vector3 targetPosition;
    private float jumpSecondsCounter;

    private PlayerAnimationController animationController;

    public float JumpHeightMultiplier { get; set; }
    public float PlayerSpeed
    {
        get
        {
            return forwardMovementSpeed;
        }
    }

    void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
        sidewayMovementOffset = SpawnManager.Instance.CellWidth;
        targetPosition = transform.position;
        playerRB = GetComponent<Rigidbody>();
        animationController = GetComponent<PlayerAnimationController>();
        JumpHeightMultiplier = 1f;
        playerHealth = GetComponent<PlayerHealth>();
    }

    void Start()
    {
        distToGround = GetComponent<Collider>().bounds.extents.y;

        currentTimeAcceleration = 0f;
    }

    void FixedUpdate()
    {
        if (GameStateManager.Instance.State != GameStates.InGame || playerHealth.IsPlayerDead)
        {
            return;
        }

        if (playerInput.Left())
        {
            MoveLeft();
        }

        if (playerInput.Right())
        {
            MoveRight();
        }

        if (playerInput.Jump() && !lockedYAxis && jumpSecondsCounter <= 0)
        {
            Jump();

            jumpSecondsCounter = jumpOnceEverySeconds;
        }

        if (playerInput.Slide() && !lockedYAxis)
        {
            TrackManager.Instance.Slide();

            if (IsGrounded())
            {
                animationController.Slide();
            }
            else
            {
                playerRB.AddForce(new Vector3(0f, -GetJumpForce(), 0f));
            }
        }

        DecreaseJumpTimer();
        MoveForward();
        SmoothSideMove();
        Acceleration();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Collectable")
        {
            var executable = other.GetComponent<Executable>();

            executable.Execute(gameObject);
        }
    }

    public bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.2f);
    }

    public void Run()
    {
        lockedYAxis = false;

        animationController.Run();
    }

    public void Swim()
    {
        lockedYAxis = true;

        animationController.Swim();
    }

    public bool CanMoveLeft()
    {
        return currentPlayerCorridor > 1;
    }

    public bool CanMoveRight()
    {
        return currentPlayerCorridor < MapManager.Instance.WalkableCorridors;
    }

    void Jump()
    {
        if (IsGrounded())
        {
            TrackManager.Instance.Jump();

            playerRB.AddForce(new Vector3(0f, GetJumpForce(), 0f));

            animationController.Jump();
        }
    }

    void MoveForward()
    {
        float distance = forwardMovementSpeed * Time.fixedDeltaTime;
        transform.SetPositionZ(transform.position.z + distance);
    }

    void MoveLeft()
    {
        if (!CanMoveLeft())
        {
            return;
        }

        TrackManager.Instance.MoveLeft();

        animationController.Left();

        currentPlayerCorridor--;
        targetPosition.x -= sidewayMovementOffset;
    }

    void MoveRight()
    {
        if (!CanMoveRight())
        {
            return;
        }

        TrackManager.Instance.MoveRight();

        animationController.Right();

        currentPlayerCorridor++;
        targetPosition.x += sidewayMovementOffset;
    }

    void SmoothSideMove()
    {
        float multiplier = ItemManager.Instance.GetBuffValue(ChangeCorridorSpeed);
        float offsetX = Mathf.Lerp(transform.position.x, targetPosition.x, multiplier * sidewayMovementSpeed * Time.fixedDeltaTime);

        transform.SetPositionX(offsetX);
    }

    void Acceleration()
    {
        currentTimeAcceleration += Time.fixedDeltaTime;

        if(currentTimeAcceleration >= timeAccelerationInterval)
        {
            forwardMovementSpeed += accelerationIntervalSpeed;

            forwardMovementSpeed = Mathf.Min(forwardMovementMaxSpeed, forwardMovementSpeed);

            currentTimeAcceleration = 0f;
        }
    }

    float GetJumpForce()
    {
        return jumpForce * JumpHeightMultiplier;
    }

    void DecreaseJumpTimer()
    {
        jumpSecondsCounter -= Time.fixedDeltaTime;

        jumpSecondsCounter = Mathf.Max(jumpSecondsCounter, 0f);
    }
}
