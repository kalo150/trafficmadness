﻿using UnityEngine;

namespace GameUtils
{
    /// <summary>
    /// Utils for math calculation.
    /// </summary>
    public class MathUtils
    {
        public static bool RandomWithPercentage(float percentage)
        {
            float randomPercent = Random.Range(1f, 101f);

            return randomPercent < percentage;
        }

        public static uint Max(uint first, uint second)
        {
            return first > second ? first : second;
        }
    }
}
