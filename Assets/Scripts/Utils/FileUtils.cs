﻿using System.IO;

namespace GameUtils
{
    /// <summary>
    /// Utils for working with files.
    /// </summary>
    public static class FileUtils
    {
        public static string Read(string fileName)
        {
            if (!File.Exists(fileName))
            {
                return null;
            }

            return File.ReadAllText(fileName);
        }

        public static void Write(string fileName, string content)
        {
            File.WriteAllText(fileName, content);
        }

        public static void Delete(string filename)
        {
            File.Delete(filename);
        }
    }
}
