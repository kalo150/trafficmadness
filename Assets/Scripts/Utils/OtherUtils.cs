﻿namespace GameUtils
{
    /// <summary>
    /// Common used utils, without specific category.
    /// </summary>
    public class OtherUtils
    {
        public static void Swap<T>(ref T first, ref T second)
        {
            T temp = first;
            first = second;
            second = temp;
        }
    }
}