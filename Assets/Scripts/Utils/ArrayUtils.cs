﻿using System.Collections.Generic;
using System.Linq;

namespace GameUtils
{
    /// <summary>
    /// Utils for list and arrays.
    /// </summary>
    public class ArrayUtils
    {
        public static List<T> CreateList<T>(int size, T value = default(T))
        {
            return Enumerable.Repeat(value, size).ToList();
        }
    }
}
