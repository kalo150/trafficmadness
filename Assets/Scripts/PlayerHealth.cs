﻿using UnityEngine;

public class PlayerHealth : MonoBehaviour 
{
    [SerializeField]
    private GameObject onDieEffect;
    [SerializeField]
    private GameObject onWinEffect;

    private bool isPlayerDead;

    public bool IsPlayerDead { get { return isPlayerDead; } }

    void Start()
    {
        isPlayerDead = false;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag != "Damageable" || isPlayerDead)
        {
            return;
        }

        Die();
    }

    void Die()
    {
        isPlayerDead = true;

        ItemManager.Instance.RemovePlayerItems(gameObject);

        if (GameInfoManager.Instance.IsPlayerBot(gameObject))
        {
            GameInfoManager.Instance.RemovePlayer(gameObject);

            Destroy(gameObject);

            if(GameInfoManager.Instance.PlayerCount == 1)
            {
                var localPlayer = GameInfoManager.Instance.Players[0];

                localPlayer.GetComponent<PlayerHealth>().OnLocalPlayerWin();
            }
        }
        else
        {
            OnLocalPlayerDie();
        }
    }

    void OnLocalPlayerWin()
    {
        var position = onWinEffect.transform.position + gameObject.transform.position;

        SpawnManager.Instance.SpawnObject(onWinEffect, position);

        GetComponent<PlayerAnimationController>().Idle();
    }

    private void OnLocalPlayerDie()
    {
        var position = onDieEffect.transform.position + gameObject.transform.position;

        SpawnManager.Instance.SpawnObject(onDieEffect, position);
    }
}
