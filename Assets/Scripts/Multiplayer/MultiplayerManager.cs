﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MultiplayerManager : StrictSingleton<MultiplayerManager> 
{
    [SerializeField]
    private GameObject offlineObject;
    [SerializeField]
    private GameObject onlineObject;

    private NetworkManager networkManager;

    new void Awake()
    {
        base.Awake();

        networkManager = GetComponent<NetworkManager>();
    }

    public void ActiveMultiplayer()
    {
        onlineObject.SetActive(true);
        offlineObject.SetActive(false);

        networkManager.enabled = true;
    }

    public void DeactiveMultiplayer()
    {
        onlineObject.SetActive(false);
        offlineObject.SetActive(true);

        networkManager.enabled = false;
    }
}
