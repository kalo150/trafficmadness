﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;

/// <summary>
/// Displaying active missions UI.
/// </summary>
public class ActiveMissionsUI : MonoBehaviour 
{
    [SerializeField]
    private GameObject missionObject;
    [SerializeField]
    private float missionOffset;

    private const string descriptionObject = "Description";
    private const string rewardObject = "Reward";
    private const string progressObject = "Progress";

    public void DrawMissions(RectTransform rect)
    {
        var missions = MissionManager.Instance.ActiveMissions;

        for(int i = 0; i < missions.Count; i++)
        {
            var mission = missions[i];

            var missionUI = Instantiate(missionObject);
            missionUI.transform.SetParent(rect, false);
            
            var missionRect = missionUI.GetComponent<RectTransform>();
            var newPositionY = i * (-missionRect.sizeDelta.y + missionOffset);
            missionRect.anchoredPosition = new Vector2(missionRect.anchoredPosition.x, newPositionY);

            SetChildTextByName(missionUI, descriptionObject, mission.Description);
            SetChildTextByName(missionUI, rewardObject, mission.GetReward());
            SetChildTextByName(missionUI, progressObject, mission.GetProgress()); ;
        }
    }

    public void SetChildTextByName(GameObject parent, string name, string content)
    {
        parent.GetComponentsInChildren<Text>()
            .First(comp => comp.name == name).text = content;
    }
}