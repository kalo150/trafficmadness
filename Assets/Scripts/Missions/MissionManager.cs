﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Keeping track of active missions 
/// and available missions.
/// </summary>
public class MissionManager : Singleton<MissionManager> 
{
    [SerializeField]
    private List<MissionWrapper> missions;
    [SerializeField]
    private int numberActiveMissions;

    private HashSet<Mission> inProgressMissions;
    private ActiveMissionsUI activeMissionsUI;

    public List<Mission> ActiveMissions
    {
        get
        {
            UpdateMissions();

            return inProgressMissions.ToList();
        }
    }

    public ActiveMissionsUI MissionUI
    {
        get
        {
            return activeMissionsUI;
        }
    }

    void Awake()
    {
        LoadActiveMissions();

        activeMissionsUI = GetComponent<ActiveMissionsUI>();
    }

    void UpdateMissions()
    {
        var activeMissions = inProgressMissions
            .Where(mission => !mission.IsCompleted())
            .ToList();
            
        foreach (var mission in activeMissions)
        {
            mission.UpdateStatus();

            if(mission.IsCompleted())
            {
                mission.Reward();
            }
        }

        if(ShouldChangeActiveMissions())
        {
            SetNewMissions();
        }
    }

    bool ShouldChangeActiveMissions()
    {
        bool allMissionsCompleted = inProgressMissions.All(mission => mission.IsCompleted());

        return (allMissionsCompleted && GameStateManager.Instance.State == GameStates.Menu)
            || inProgressMissions.Count == 0;
    }

    void LoadActiveMissions()
    {
        var savedData = UserManager.Instance.GetMissionProgress();
        inProgressMissions = new HashSet<Mission>();

        savedData.ForEach(data => inProgressMissions.Add(data));
    }

    void SetNewMissions()
    {
        inProgressMissions.Clear();

        var shuffledMissions = missions.ToList();
        shuffledMissions.Shuffle();

        for (int i = 0; i < numberActiveMissions; i++)
        {
            var newMission = shuffledMissions[i].GetMission();

            inProgressMissions.Add(newMission);
        }
    }
}