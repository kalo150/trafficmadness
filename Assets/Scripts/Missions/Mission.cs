﻿using UnityEngine;

/// <summary>
/// Wrapper for the inspector.
/// </summary>
public abstract class MissionWrapper : MonoBehaviour
{
    public abstract Mission GetMission();
}

[System.Serializable]
public abstract class Mission : System.ICloneable
{
    [SerializeField]
    protected string description;

    public string Description { get { return description; } }

    public abstract string GetProgress();
    public abstract string GetReward();
    public abstract bool IsCompleted();
    public abstract void UpdateStatus();
    public abstract object Clone();
    public abstract void Reward();
}