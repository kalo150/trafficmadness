﻿public class CountMissionWrapper : MissionWrapper 
{
    [UnityEngine.SerializeField]
    private CountMission mission;

    public override Mission GetMission()
    {
        return mission.Clone() as Mission;
    }
}