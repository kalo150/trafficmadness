﻿using UnityEngine;

/// <summary>
/// Mission type which counts times done some activity, 
/// and gives coins as a reward.
/// </summary>
[System.Serializable]
public class CountMission : Mission
{
    public enum CountMissionType
    {
        Slides,
        Jumps,
        CollectItems,
        ChangeLane
    }

    [SerializeField]
    private int targetNumber;
    [SerializeField]
    private CountMissionType type;
    [SerializeField]
    private int currentCount;
    [SerializeField]
    private uint reward;

    public int TargetNumber
    {
        get { return targetNumber;  }
        set { targetNumber = value; }
    }

    public CountMissionType Type
    {
        get { return type;  }
        set { type = value; }
    }

    public int CurrentCount
    {
        get { return currentCount;  }
        set { currentCount = value; }
    }

    public override string GetProgress()
    {
        return currentCount.ToString() + " from " + targetNumber.ToString();
    }

    public override string GetReward()
    {
        return "Reward: " + reward.ToString();
    }

    public override bool IsCompleted()
    {
        return currentCount >= targetNumber;
    }

    public override void UpdateStatus()
    {
        currentCount += TrackManagerCount();

        currentCount = Mathf.Min(currentCount, targetNumber);
    }

    public override object Clone()
    {
        return new CountMission()
        {
            targetNumber = targetNumber,
            type = type,
            currentCount = currentCount,
            description = description,
            reward = reward
        };
    }

    public override void Reward()
    {
        UserManager.Instance.AddCoins(reward);
    }

    int TrackManagerCount()
    {
        switch (type)
        {
            case CountMissionType.Slides:
                return TrackManager.Instance.Slides;
            case CountMissionType.Jumps:
                return TrackManager.Instance.Jumps;
            case CountMissionType.CollectItems:
                return TrackManager.Instance.ItemCollected;
            default:
                return TrackManager.Instance.LaneChanges;
        }
    }
}