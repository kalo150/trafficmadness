﻿using UnityEngine;

/// <summary>
/// Change player control and animation state.
/// Depending on the previous state.
/// </summary>
public class WaterStateChange : MonoBehaviour 
{
    [SerializeField]
    private bool changeToNormalState;

    void OnTriggerEnter(Collider other)
    {
        if(other.tag != "Player")
        {
            return;
        }

        if(changeToNormalState)
        {
            other.GetComponent<PlayerControl>().Run();
        }
        else
        {
            other.GetComponent<PlayerControl>().Swim();
        }
    }
}
