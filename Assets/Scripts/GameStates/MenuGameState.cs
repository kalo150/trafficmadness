﻿using UnityEngine;

public class MenuGameState : GameStateHandler 
{
    [SerializeField]
    private GameObject inGameCanvas;

    private bool startGame;

    public override void Execute(GameStateStack gameStateStack)
    {
        if(startGame)
        {
            startGame = false;

            UIManager.Instance.SetPrimaryCanvas(inGameCanvas);

            gameStateStack.ReplaceCurrentState(GameStates.InGame);
        }
    }

    public void StartGame()
    {
        startGame = true;
    }
}
