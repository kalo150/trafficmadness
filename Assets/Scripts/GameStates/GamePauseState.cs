﻿using UnityEngine.SceneManagement;

public class GamePauseState : GameStateHandler 
{
    private bool resumeGame;

    public override void Execute(GameStateStack gameStateStack)
    {
        if (resumeGame)
        {
            resumeGame = false;

            UIManager.Instance.GoBack();

            gameStateStack.RemoveCurrentState();
        }
    }

    public void OnResumeButtonPress()
    {
        resumeGame = true;
    }

    public void OnBackToMain()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
