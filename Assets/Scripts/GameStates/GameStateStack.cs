﻿using System.Collections.Generic;

public class GameStateStack
{
    private Stack<GameStates> stateStack;

    public GameStateStack(GameStates startState)
    {
        stateStack = new Stack<GameStates>();

        stateStack.Push(startState);
    }

    public void AddState(GameStates state)
    {
        stateStack.Push(state);
    }

    public void ReplaceCurrentState(GameStates state)
    {
        stateStack.Pop();

        stateStack.Push(state);
    }

    public void RemoveCurrentState()
    {
        stateStack.Pop();
    }

    public GameStates GetActiveState()
    {
        return stateStack.Peek();
    }
}
