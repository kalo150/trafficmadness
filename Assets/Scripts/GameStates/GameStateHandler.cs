﻿using UnityEngine;

public abstract class GameStateHandler : MonoBehaviour
{
    public abstract void Execute(GameStateStack gameStateStack);
}
