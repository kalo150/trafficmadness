﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public enum GameStates
{
    Menu, 
    InGame,
    Pause,
    GameOver,
    GameWin
}

public class GameStateManager : Singleton<GameStateManager> 
{
    [System.Serializable]
    private class KeyValueStates
    {
        public GameStates state;
        public GameStateHandler handler;
    }

    [SerializeField]
    private GameStates defaultState;
    [SerializeField]
    private List<KeyValueStates> handlersList;

    private GameStateStack gameStateStack;

    public GameStates State
    {
        get
        {
            if(gameStateStack == null)
            {
                gameStateStack = new GameStateStack(defaultState);
            }

            return gameStateStack.GetActiveState();
        }
    }

    private Dictionary<GameStates, GameStateHandler> handlers;

    void Awake()
    {
        handlers = handlersList.ToDictionary(list => list.state, list => list.handler);
    }
    
    void FixedUpdate()
    {
        handlers[State].Execute(gameStateStack);
    }
}
