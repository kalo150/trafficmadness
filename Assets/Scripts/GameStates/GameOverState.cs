﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameOverState : GameStateHandler 
{
    private bool gameOverCoroutineRunning;

    void Start()
    {
        gameOverCoroutineRunning = false;
    }

    public override void Execute(GameStateStack gameStateStack)
    {
        if (gameOverCoroutineRunning)
        {
            return;
        }

        gameOverCoroutineRunning = true;

        StartCoroutine("OnGameOver", 1f);
    }

    IEnumerator OnGameOver(float restartAfterSeconds)
    {
        UserManager.Instance.SaveNewUserData();

        yield return new WaitForSeconds(restartAfterSeconds);

        var async = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);

        yield return async;
    }
}
