﻿using UnityEngine;

public class InGameState : GameStateHandler 
{
    [SerializeField]
    private PlayerHealth playerHealth;
    [SerializeField]
    private GameObject pauseCanvas;

    private bool pauseGame;

    public override void Execute(GameStateStack gameStateStack)
    {
        if (playerHealth.IsPlayerDead)
        {
            gameStateStack.ReplaceCurrentState(GameStates.GameOver);
        }

        if(pauseGame)
        {
            pauseGame = false;

            UIManager.Instance.AddCanvas(pauseCanvas);

            gameStateStack.AddState(GameStates.Pause);
        }

        if (PlayerWonCompetitive())
        {
            gameStateStack.ReplaceCurrentState(GameStates.GameWin);
        }
    }

    public void OnPauseButtonPress()
    {
        pauseGame = true;
    }

    bool PlayerWonCompetitive()
    {
        return ModeManager.Instance.CurrentMode == ModeManager.Mode.Competitive &&
            GameInfoManager.Instance.PlayerCount == 1;
    }
}
