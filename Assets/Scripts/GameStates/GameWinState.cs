﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameWinState : GameStateHandler 
{
    private bool gameWinCoroutineRunning;

    [SerializeField]
    private uint reward;

    void Start()
    {
        gameWinCoroutineRunning = false;
    }

    public override void Execute(GameStateStack gameStateStack)
    {
        if (gameWinCoroutineRunning)
        {
            return;
        }

        gameWinCoroutineRunning = true;

        ScoreManager.Instance.AddCoin(reward);

        StartCoroutine("OnGameWin", 1f);
    }

    IEnumerator OnGameWin(float restartAfterSeconds)
    {
        UserManager.Instance.SaveNewUserData();

        yield return new WaitForSeconds(restartAfterSeconds);

        var async = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);

        yield return async;
    }
}
