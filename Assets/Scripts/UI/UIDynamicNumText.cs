﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Abstarct class controlling UI text element.
/// </summary>
public abstract class UIDynamicNumText : MonoBehaviour 
{
    private Text text;

    private uint prevNum;

    [SerializeField]
    private string prefixMessage;

    void Start()
    {
        text = GetComponent<Text>();
    }

    void Update()
    {
        uint currentNum = GetNumValue();

        if (prevNum == currentNum)
        {
            return;
        }

        text.text = prefixMessage + currentNum.ToString();

        prevNum = currentNum;
    }
    
    protected abstract uint GetNumValue();
}
