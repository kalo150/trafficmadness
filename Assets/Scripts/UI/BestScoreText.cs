﻿/// <summary>
/// Displays best score.
/// </summary>
public class BestScoreText : UIDynamicNumText
{
    private uint bestScore;

    protected override uint GetNumValue()
    {
        return bestScore;
    }

    void OnEnable() 
	{
        bestScore = ScoreManager.Instance.GetBestScore();
	}
}
