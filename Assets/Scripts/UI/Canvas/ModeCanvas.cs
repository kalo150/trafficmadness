﻿public class ModeCanvas : CanvasController 
{
    public void SetFreeRun()
    {
        ModeManager.Instance.SetMode(ModeManager.Mode.FreeRun);

        DisableCanvas();
    }

    public void SetCompetitive()
    {
        ModeManager.Instance.SetMode(ModeManager.Mode.Competitive);

        DisableCanvas();
    }

    public void SetMultiplayer()
    {
        ModeManager.Instance.SetMode(ModeManager.Mode.Multiplayer);

        DisableCanvas();
    }
}
