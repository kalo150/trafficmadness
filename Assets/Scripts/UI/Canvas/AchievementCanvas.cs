﻿using UnityEngine;

public class AchievementCanvas : CanvasController
{
    [SerializeField]
    private RectTransform content;

    void OnEnable()
    {
        MissionManager.Instance.MissionUI.DrawMissions(content);
    }

    void OnDisable()
    {
        content.transform.DestroyChilds();
    }
}