﻿using UnityEngine;
using UnityEngine.UI;

public class SettingsCanvas : CanvasController 
{
    private const string onString = "On";
    private const string offString = "Off";

    [SerializeField]
    private Text soundText;
    [SerializeField]
    private GameObject loginField;

    protected override void OnCanvasEnable()
    {
        HandleLoginButton();
        loginField.SetActive(UserManager.Instance.IsLoggedIn());
    }

    public void Logout()
    {
        UserManager.Instance.UserLogout();

        UIManager.Instance.GoBack();
    }

    public void ToggleSound()
    {
        SetSoundText();

        AudioManager.Instance.IsSoundOn = !AudioManager.Instance.IsSoundOn;
    }

    void SetSoundText()
    {
        soundText.text = "Turn " + (AudioManager.Instance.IsSoundOn ? onString : offString);
    }

    void HandleLoginButton()
    {
        loginField.SetActive(UserManager.Instance.IsLoggedIn());
    }
}
