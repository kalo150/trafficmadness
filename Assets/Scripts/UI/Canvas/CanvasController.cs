﻿using UnityEngine;

public class CanvasController : MonoBehaviour 
{
    protected virtual void OnCanvasEnable() { }

    public void EnableCanvas()
    {
        gameObject.SetActive(true);

        UIManager.Instance.AddCanvas(gameObject);

        OnCanvasEnable();
    }

    public void DisableCanvas()
    {
        gameObject.SetActive(false);

        UIManager.Instance.GoBack();
    }
}
