﻿using UnityEngine;
using System.Collections;

public class PauseCanvas : CanvasController
{
    [SerializeField]
    private RectTransform content;

    void OnEnable()
    {
        MissionManager.Instance.MissionUI.DrawMissions(content);
    }

    void OnDisable()
    {
        content.transform.DestroyChilds();
    }
}