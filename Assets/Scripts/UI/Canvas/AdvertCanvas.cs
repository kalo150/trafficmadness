﻿using System;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;

public class AdvertCanvas : CanvasController
{
    private const string advertCanvasCache = "advertCanvasCache";
    [SerializeField]
    private Button watchButton;
    [SerializeField]
    private uint onFullWatchReward;
    [SerializeField]
    private uint onSkipWatchReward;

#if UNITY_ANDROID
    private DateTime LastDateUpdated
    {
        get
        {
            return (DateTime) CacheManager.Instance.GetValue(advertCanvasCache);
        }

        set
        {
            CacheManager.Instance.SaveValue(advertCanvasCache, value);
        }
    }

    protected override void OnCanvasEnable()
    {
        watchButton.interactable = LastDateUpdated.Day != DateTime.Now.Day;
    }

    public void OnWatchButtonClick()
    {
        var options = new ShowOptions();
        options.resultCallback = RewardCallback;

        if (Advertisement.IsReady())
        {
            Advertisement.Show(null, options);

            DisableCanvas();
        }
    }

    void RewardCallback(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Finished:
                UserManager.Instance.AddCoins(onFullWatchReward);
                break;
            case ShowResult.Skipped:
                UserManager.Instance.AddCoins(onSkipWatchReward);
                break;
        }

        if(result != ShowResult.Failed)
        {
            LastDateUpdated = DateTime.Now;
        }
    }
#endif
}
