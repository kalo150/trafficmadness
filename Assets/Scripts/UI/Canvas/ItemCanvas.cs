﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ItemCanvas : CanvasController 
{
    private const string levelProgressObject = "LevelProgress";
    private const string thumbnailObject = "Thumbnail";
    private const string priceObject = "Price";
    private const string nameObject = "Name";
    private const string buyObject = "Buy";

    [SerializeField]
    private Text coins;
    [SerializeField]
    private RectTransform itemContent;
    [SerializeField]
    private GameObject itemPrefab;
    [SerializeField]
    private float heightOffset;
    [SerializeField]
    private GameObject filledLevel;
    [SerializeField]
    private GameObject unfilledLevel;
    [SerializeField]
    private float widthLevelProgressOffset;

    void OnEnable()
    {
        UpdateCoins();

        var items = ItemManager.Instance.GetItems();

        for(int i = 0; i < items.Count; i++)
        {
            ShowItem(items[i], i);
        }
    }

    void OnDisable()
    {
        itemContent.transform.DestroyChilds();
    }

    void ShowItem(Item item, int position)
    {
        // Create item
        var prefab = Instantiate(itemPrefab);
        prefab.transform.SetParent(itemContent.transform, false);

        itemContent.sizeDelta = new Vector2(itemContent.sizeDelta.x, Mathf.Abs((position + 1) * heightOffset));
        var prefabRect = prefab.GetComponent<RectTransform>();
        var newPosition = new Vector2(prefabRect.anchoredPosition.x, prefabRect.anchoredPosition.y + (position * heightOffset));
        prefabRect.anchoredPosition = newPosition;

        var imageComponents = prefab.GetComponentsInChildren<Image>();
        var textComponents = prefab.GetComponentsInChildren<Text>();
        var buttonComponents = prefab.GetComponentsInChildren<Button>();

        // Set thumbnail
        imageComponents.First(img => img.name == thumbnailObject)
            .sprite = item.Thumbnail;

        // Set item name
        textComponents.First(txt => txt.name == nameObject)
            .text = item.Type;

        // Set item price
        textComponents.First(txt => txt.name == priceObject)
            .text = ItemManager.Instance.GetPrice(item.Level).ToString();

        // Set item level
        var levelProgress = imageComponents.First(img => img.name == levelProgressObject).rectTransform;
        var numberLevels = ItemManager.Instance.CountLevels();
        levelProgress.DestroyChilds();

        var widthSingleLevel = (levelProgress.sizeDelta.x / numberLevels) - widthLevelProgressOffset;
        for (int i = 0; i < numberLevels; i++)
        {
            var levelStatus = Instantiate(item.Level <= i ? unfilledLevel : filledLevel);
            levelStatus.transform.SetParent(levelProgress.transform, false);

            var rectLevelStatus = levelStatus.GetComponent<RectTransform>();
            rectLevelStatus.sizeDelta = new Vector2(widthSingleLevel, rectLevelStatus.sizeDelta.y);
            var newPositionX = (i + 1) * widthLevelProgressOffset + i * widthSingleLevel;
            rectLevelStatus.anchoredPosition = new Vector2(newPositionX, rectLevelStatus.anchoredPosition.y);
        }

        // Set listener, on buy click
        if(item.Level < ItemManager.Instance.CountLevels())
        {
            buttonComponents.First(button => button.name == buyObject).onClick.AddListener(() => {
                TryToBuy(item);
            });
        }
    }

    void UpdateCoins()
    {
        coins.text = UserManager.Instance.GetCoins().ToString();
    }

    void TryToBuy(Item item)
    {
        var levelCost = ItemManager.Instance.GetPrice(item.Level);
        if (UserManager.Instance.GetCoins() >= levelCost)
        {
            item.Level = item.Level + 1;
            uint newNumberCoins = UserManager.Instance.GetCoins() - levelCost;

            UserManager.Instance.UpdateUserCoins(newNumberCoins);
            ItemManager.Instance.SaveItem(item);

            OnEnable();
        }

    }
}
