﻿using UnityEngine;

public class MenuCanvas : CanvasController
{
    public void QuitGame()
    {
        Application.Quit();
    }
}
