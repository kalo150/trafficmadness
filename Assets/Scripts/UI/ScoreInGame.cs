﻿/// <summary>
/// Displays ingame score or players alive, depending on the game mode.
/// </summary>
public class ScoreInGame : UIDynamicNumText 
{
    protected override uint GetNumValue()
    {
        if(ModeManager.Instance.CurrentMode == ModeManager.Mode.FreeRun)
        {
            return ScoreManager.Instance.Score;
        }
        else
        {
            return (uint) (GameInfoManager.Instance.PlayerCount) - 1;
        }
    }    
}
