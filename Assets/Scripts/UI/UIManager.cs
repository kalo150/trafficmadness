﻿using UnityEngine;

/// <summary>
/// Managing UI components, and controlling UI stack.
/// </summary>
public class UIManager : Singleton<UIManager> 
{
    [SerializeField]
    private GameObject startingCanvas;

    private CanvasStack canvasStack;
    
    void Start()
    {
        canvasStack = new CanvasStack();

        canvasStack.AddCanvas(startingCanvas);
    }

    public void AddCanvas(GameObject canvas)
    {
        canvasStack.AddCanvas(canvas);
    }

    public void SetPrimaryCanvas(GameObject canvas)
    {
        canvasStack.DisableAndClearCanvas();

        canvasStack.AddCanvas(canvas);
    }

    public void GoBack()
    {
        canvasStack.GoPrevCanvas();
    }
}
