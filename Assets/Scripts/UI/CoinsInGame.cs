﻿/// <summary>
/// Displays ingame coins.
/// </summary>
public class CoinsInGame : UIDynamicNumText 
{
    protected override uint GetNumValue()
    {
        return ScoreManager.Instance.Coins;
    }
}
