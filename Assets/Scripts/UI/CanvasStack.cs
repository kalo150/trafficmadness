﻿using UnityEngine;
using System.Collections.Generic;

public class CanvasStack
{
    private Stack<GameObject> canvasStack;

    public CanvasStack()
    {
        canvasStack = new Stack<GameObject>();
    }

    public void AddCanvas(GameObject canvas)
    {
        if(canvasStack.Count > 0)
        {
            canvasStack.Peek().SetActive(false);
        }

        canvas.SetActive(true);

        canvasStack.Push(canvas);
    }

    public void GoPrevCanvas()
    {
        canvasStack.Pop().SetActive(false);

        canvasStack.Peek().SetActive(true);
    }

    public void ReplaceCurrentCanvas(GameObject newCanvas)
    {
        canvasStack.Pop().SetActive(false);

        newCanvas.SetActive(true);

        canvasStack.Push(newCanvas);
    }

    public void DisableAndClearCanvas()
    {
        while(canvasStack.Count > 0)
        {
            canvasStack.Pop().SetActive(false);
        }
    }
}
