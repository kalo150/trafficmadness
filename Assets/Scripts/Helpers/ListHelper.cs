﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public static class ListHelper
{
	public static List<T> Clone<T>(this List<T> list)
    {
        return list.ToList();
    }

    public static bool ValidIndex<T>(this List<T> list, int index)
    {
        return index >= 0 && index < list.Count; 
    }

    public static T RandomValue<T>(this List<T> list)
    {
        int randomIndex = Random.Range(0, list.Count);

        return list[randomIndex];
    }

    public static T RandomWithPriority<T>(this List<T> list) where T : PriorityItem
    {
        int sumPriorities = 0;

        foreach (var item in list)
        {
            sumPriorities += item.Priority;
        }

        int randomPriority = Random.Range(0, sumPriorities + 1);
        int total = 0;

        foreach (var item in list)
        {
            total += item.Priority;

            if (total >= randomPriority)
            {
                return item;
            }
        }

        return null;
    }

    public static void Shuffle<T>(this List<T> list)
    {
        var random = new System.Random();

        for(int i = 0; i < list.Count; i++)
        {
            var randomIndex = random.Next(list.Count);

            list.Swap(i, randomIndex);
        }
    }

    public static void Swap<T>(this List<T> list, int firstIndex, int secondIndex)
    {
        T temp = list[firstIndex];
        list[firstIndex] = list[secondIndex];
        list[secondIndex] = temp;
    }
}
