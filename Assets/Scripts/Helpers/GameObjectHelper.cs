﻿using UnityEngine;
using System.Linq;

public static class GameObjectHelper
{
    public static bool IsDestroyed(this GameObject gameObject)
    {
        return gameObject == null && !ReferenceEquals(gameObject, null);
    }
}
