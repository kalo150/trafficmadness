﻿using UnityEngine;

public static class TransformHelper
{
    #region scale

    public static void SetScaleX(this Transform transform, float value)
    {
        Vector3 vector = transform.localScale;
        transform.localScale = new Vector3(value, vector.y, vector.z);
    }

    #endregion
    #region position

    public static void SetPositionZ(this Transform transform, float value)
    {
        Vector3 vector = transform.position;
        transform.position = new Vector3(vector.x, vector.y, value);
    }

    public static void SetPositionX(this Transform transform, float value)
    {
        Vector3 vector = transform.position;
        transform.position = new Vector3(value, vector.y, vector.z);
    }

    public static void SetPositionY(this Transform transform, float value)
    {
        Vector3 vector = transform.position;
        transform.position = new Vector3(vector.x, value, vector.z);
    }

    #endregion
    #region other
    public static void DestroyChilds(this Transform transform)
    {
        foreach(Transform child in transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
    #endregion
}