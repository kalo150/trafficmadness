﻿using System.Collections.Generic;

public static class DictionaryHelper
{
    public static void AddOrReplace<T, V>(this Dictionary<T, V> dict, T key, V value)
    {
        if(dict.ContainsKey(key))
        {
            dict[key] = value;
        }
        else
        {
            dict.Add(key, value);
        }
    }
}
