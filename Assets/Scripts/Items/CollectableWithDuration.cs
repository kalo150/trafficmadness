﻿using UnityEngine;

public class CollectableWithDuration : Item, Executable
{
    [SerializeField]
    private GameObject onDestroyParticle;

    protected virtual void OnItemEnable() { }

    protected GameObject player;

    protected void DestroyItem()
    {
        Destroy(gameObject);
    }

    public bool Execute(GameObject player)
    {
        this.player = player;

        OnItemEnable();

        SpawnManager.Instance.SpawnObject(onDestroyParticle, transform.position);

        return true;
    }
}
