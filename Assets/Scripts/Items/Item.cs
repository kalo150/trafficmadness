﻿using UnityEngine;

[System.Serializable]
public class ItemSerializable
{
    [SerializeField]
    public string type;
    [SerializeField]
    public int level;
}

public class Item : MonoBehaviour
{
    [SerializeField]
    private int level;
    [SerializeField]
    private string type;
    [SerializeField]
    private float duration;
    [SerializeField]
    private float bonusDurationEachLevel;
    [SerializeField]
    private Sprite thumbnail;

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Damageable")
        {
            Destroy(gameObject);
        }
    }

    public string Type
    {
        get { return type;  }
        set { type = value; }
    }

    public float Duration
    {
        get { return duration;  }
        set { duration = value; }
    }

    public int Level
    {
        get { return level;  }
        set { level = value; }
    }

    public Sprite Thumbnail
    {
        get { return thumbnail;  }
        set { thumbnail = value; }
    }

    public float GetFullDuration()
    {
        return level * bonusDurationEachLevel + duration;
    }

    public ItemSerializable ToSerializable()
    {
        var item = new ItemSerializable();

        item.type = type;
        item.level = level;

        return item;
    }
}
