﻿using UnityEngine;
using System.Collections.Generic;

public class Jetpack : Collectable<Jetpack>
{
    private const string mainCameraTag = "MainCamera";

    [SerializeField]
    private float playerNewXAngle;
    [SerializeField]
    private float playerYPosition;
    [SerializeField]
    private float coinsChanceChangeLane;

    private MainCamera mainCamera;
    private Rigidbody playerRigidbody;
    private PlayerAnimationController playerAnimationController;
    private Dictionary<CollectablePartGenerator.CoinPackType, Element> coinPacks;

    void Start()
    {
        mainCamera = GameObject.FindGameObjectWithTag(mainCameraTag).GetComponent<MainCamera>();
    }

    protected override void OnItemEnable()
    {
        transform.GetChild(0).gameObject.SetActive(true);

        DisableRenderAndCollider();

        ActivateItem();

        mainCamera.FollowOnlyGrounded = false;

        playerRigidbody = player.GetComponent<Rigidbody>();
        playerRigidbody.isKinematic = true;

        playerAnimationController = player.GetComponent<PlayerAnimationController>();

        playerAnimationController.Idle();

        player.transform.rotation = Quaternion.Euler(playerNewXAngle, 0f, 0f);

        player.transform.SetPositionY(playerYPosition);

        SpawnCoins();
    }

    protected override void OnDurationOver()
    {
        mainCamera.FollowOnlyGrounded = true;
        playerRigidbody.isKinematic = false;
        playerAnimationController.Run();
        player.transform.rotation = Quaternion.Euler(Vector3.zero);

        DestroyItem();
    }

    void SpawnCoins()
    {
        var coinPacks = CollectablePartGenerator.Instance.NoGravityCoinPacks;

        int sizeX = MapManager.Instance.WalkableCorridors;

        float playerSpeed = player.GetComponent<PlayerControl>().PlayerSpeed;
        int sizeY = (int) ((GetFullDuration() * playerSpeed) / SpawnManager.Instance.CellLength);

        float playerPosition = GameInfoManager.Instance.FirstPlayer.z;

        var path = new Grid<Element>(sizeX, sizeY);

        var rows = path.CountRows();
        var x = path.CountCols() / 2;

        for(int y = 0; y < rows; y++)
        {
            if(GameUtils.MathUtils.RandomWithPercentage(coinsChanceChangeLane))
            {
                if (GameUtils.MathUtils.RandomWithPercentage(50f) && path.ValidIndex(x - 1, y))
                {
                    path[y][x--] = coinPacks[CollectablePartGenerator.CoinPackType.Left];
                }
                else if(path.ValidIndex(x + 1, y))
                {
                    path[y][x++] = coinPacks[CollectablePartGenerator.CoinPackType.Right];
                }
                else
                {
                    path[y][x] = coinPacks[CollectablePartGenerator.CoinPackType.Straight];
                }
            }
            else
            {
                path[y][x] = coinPacks[CollectablePartGenerator.CoinPackType.Straight];
            }
        }

        SpawnManager.Instance.SpawnObjects(path, new Vector3(0f, playerYPosition, playerPosition));
    }
}
