﻿using UnityEngine;

public class ScoreMultiplierItem : Collectable<ScoreMultiplierItem>
{
    private const float defaultMultiplier = 1f; 

    [SerializeField]
    private float multiply;

    protected override void OnItemEnable()
    {
        ScoreManager.Instance.ScoreMultiplier = multiply;

        DisableRenderAndCollider();

        ActivateItem();
    }

    protected override void OnDurationOver()
    {
        ScoreManager.Instance.ScoreMultiplier = defaultMultiplier;

        DestroyItem();
    }
}
