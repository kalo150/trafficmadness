﻿using UnityEngine;

public class JumpBoost : Collectable<JumpBoost>
{
    private const float defaultJumpMultiplier = 1f;

    [SerializeField]
    private float jumpBoostMultiplier;

    private PlayerControl playerControl;
    
    protected override void OnItemEnable()
    {
        playerControl = player.GetComponent<PlayerControl>();

        playerControl.JumpHeightMultiplier = jumpBoostMultiplier;

        DisableRenderAndCollider();

        ActivateItem();
    }

    protected override void OnDurationOver()
    {
        playerControl.JumpHeightMultiplier = defaultJumpMultiplier;

        DestroyItem();
    }
}
