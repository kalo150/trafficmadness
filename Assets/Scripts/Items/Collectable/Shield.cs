﻿using UnityEngine;

public class Shield : Collectable<Shield>
{
    [SerializeField]
    private float topBoostUnits;

    void OnTriggerEnter(Collider other)
    {
        if(other.tag != "Damageable")
        {
            return;
        }

        player.transform.SetPositionY(player.transform.position.y + topBoostUnits);
        
        DeactivateItem();

        DestroyItem();
    }

    protected override void OnItemEnable()
    {
        DisableRenderAndCollider();

        ActivateItem();

        GetComponent<CapsuleCollider>().enabled = true;
    }

    protected override void OnDurationOver()
    {
        DestroyItem();
    }
}
