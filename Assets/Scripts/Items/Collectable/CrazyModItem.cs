﻿public class CrazyModItem : Collectable<CrazyModItem>
{
    protected override void OnItemEnable()
    {
        CollectablePartGenerator.Instance.CrazyModActive = true;

        DisableRenderAndCollider();

        ActivateItem();
    }

    protected override void OnDurationOver()
    {
        CollectablePartGenerator.Instance.CrazyModActive = false;

        DestroyItem();
    }
}