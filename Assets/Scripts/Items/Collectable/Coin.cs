﻿using UnityEngine;

public class Coin : CollectableWithDuration 
{
    [SerializeField]
    private uint reward;
    [SerializeField]
    private AudioClip onCollectSound;

    protected override void OnItemEnable()
    {
        if(!GameInfoManager.Instance.IsPlayerBot(player))
        {
            ScoreManager.Instance.AddCoin(reward);
        }

        AudioManager.Instance.PlayAudioClip(onCollectSound);

        DestroyItem();
    }
}
