﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class MagnetItem : Collectable<MagnetItem>
{
    private static HashSet<GameObject> magnetedCoins = new HashSet<GameObject>();

    [SerializeField]
    private Vector3 coinTargetOffset;

    protected override void OnItemUpdate()
    {
        UpdateCoinForce();
    }

    protected override void OnItemEnable()
    {
        DisableRenderAndCollider();

        GetComponent<CapsuleCollider>().enabled = true;

        ActivateItem();
    }

    protected override void OnDurationOver()
    {
        if (IsItemActive())
        {
            GetComponent<CapsuleCollider>().enabled = false;
        }

        if (magnetedCoins.Count == 0)
        {
            DestroyItem();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Coins")
        {
            MagnetizeCoin(other);
        }
    }

    void UpdateCoinForce()
    {
        foreach(var coin in magnetedCoins.ToList())
        {
            if (coin.IsDestroyed())
            {
                magnetedCoins.Remove(coin);
            }
            else
            {
                MoveMagnetedCoin(coin);
            }
        }
    }

    void MoveMagnetedCoin(GameObject coin)
    {
        var coinPosition = coin.gameObject.transform.position;
        var playerPosition = player.transform.position + coinTargetOffset;
        var force = playerPosition - coinPosition;

        coin.transform.position = coin.transform.position + force.normalized;
    }

    void MagnetizeCoin(Collider other)
    {
        magnetedCoins.Add(other.gameObject);

        other.GetComponent<BoxCollider>().enabled = false;
    }
}
