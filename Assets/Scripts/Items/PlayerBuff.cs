﻿public abstract class PlayerBuff : Item
{
    public abstract float GetBuffValue();
}