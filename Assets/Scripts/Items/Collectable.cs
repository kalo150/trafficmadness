﻿using UnityEngine;
using System.Collections.Generic;

public class Collectable<T> : Item, Executable
{
    [SerializeField]
    private GameObject onDestroyParticle;
    [SerializeField]
    private AudioClip onCollectSound;

    private static Dictionary<GameObject, GameObject> active;
    private const string neutralTag = "Untagged";

    protected virtual void OnItemEnable() { }
    protected virtual void OnItemUpdate() { }
    protected virtual void OnDurationOver() { }

    protected GameObject player;

    private Dictionary<GameObject, GameObject> Active
    {
        get
        {
            if(active == null)
            {
                active = new Dictionary<GameObject, GameObject>();
            }

            return active;
        }
    }
    
    private bool itemCollected;

    protected void DestroyItem()
    {
        Destroy(gameObject);
    }

    protected bool IsItemActive()
    {
        if(player == null)
        {
            return false;
        }

        return Active.ContainsKey(player);
    }

    protected void DisableRenderAndCollider()
    {
        foreach (var collider in GetComponents<Collider>())
        {
            collider.enabled = false;
        }

        foreach (var renderer in GetComponents<Renderer>())
        {
            renderer.enabled = false;
        }
    }

    protected void ActivateItem()
    {
        AudioManager.Instance.PlayAudioClip(onCollectSound);

        if (Active.ContainsKey(player))
        {
            var active = Active[player];

            if (active != null && !active.IsDestroyed())
            {
                DestroyObject(active);

                Active.Remove(player);
            }
        }

        FreezeRigidbody();
        gameObject.tag = neutralTag;

        Active.AddOrReplace(player, gameObject);

        ItemManager.Instance.ActivateItem(this, player);
    }

    protected void DeactivateItem()
    {
        ItemManager.Instance.DeactivateItem(this, player);

        Active.Remove(player);
    }

    public bool Execute(GameObject player)
    {
        if(itemCollected)
        {
            return false;
        }

        itemCollected = true;

        this.player = player;

        OnItemEnable();

        SpawnManager.Instance.SpawnObject(onDestroyParticle, transform.position);


        return true;
    }

    void FixedUpdate()
    {
        if (!IsItemActive())
        {
            return;
        }

        FollowPlayer();

        if (IsItemDurationOver())
        {
            OnDurationOver();
        }

        OnItemUpdate();
    }

    void FollowPlayer()
    {
        transform.position = player.transform.position;
    }

    bool IsItemDurationOver()
    {
        return ItemManager.Instance.IsDurationOver(this, player);
    }

    void FreezeRigidbody()
    {
        GetComponent<Rigidbody>().constraints |= RigidbodyConstraints.FreezePositionY;
    }
}
