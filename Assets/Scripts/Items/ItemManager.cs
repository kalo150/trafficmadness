﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ItemManager : Singleton<ItemManager>
{
    [SerializeField]
    private List<Item> itemsList;
    [SerializeField]
    private List<uint> itemPrices;
    [SerializeField]
    private float addDurationLevel;

    private Dictionary<string, Item> items;
    private ItemDurations itemDurations;
    private ItemUIController itemUIController;

    void Awake()
    {
        itemUIController = GetComponent<ItemUIController>();

        items = itemsList.ToDictionary(item => item.Type, item => item);
    }

    void Start()
    {
        InitItems();

        itemDurations = new ItemDurations();
    }

    void FixedUpdate()
    {
        if(GameStateManager.Instance.State != GameStates.InGame)
        {
            return;
        }

        UpdateDurations();
    }

    public bool IsDurationOver(Item item, GameObject player)
    {
        return itemDurations.IsDurationOver(item, player);
    }

    public List<Item> GetItems()
    {
        return itemsList;
    }

    public Item GetItem(string itemType)
    {
        if(items.ContainsKey(itemType))
        {
            return items[itemType];
        }

        return null;
    }

    public uint GetPrice(int level)
    {
        if(itemPrices.Count > level)
        {
            return itemPrices[level];
        }
        else
        {
            LogManager.Warning("Level out of range " + level);

            return int.MaxValue;
        }
    }

    public int CountLevels()
    {
        return itemPrices.Count();
    }

    public void DeactivateItem(Item item, GameObject player)
    {
        itemDurations.EndDuration(item, player);

        TryDestroyUI(item, player);
    }

    public void ActivateItem(Item item, GameObject player)
    {
        if (items.ContainsKey(item.Type))
        {
            itemDurations.StartDuration(item, player);
        }
        else
        {
            SaveItem(item);

            LogManager.Warning("File of type " + item.Type + " wasn't found");
        }

        TrackManager.Instance.CollectItem();

        TryCreateUI(item, player);
    }

    public List<ItemSerializable> SerializableItems()
    {
        return itemsList.Select(item => item.ToSerializable()).ToList();
    }

    public void SaveItem(Item item)
    {
        items.AddOrReplace(item.Type, item);

        UserManager.Instance.SaveNewUserData();
    }

    public void RemovePlayerItems(GameObject player)
    {
        itemDurations.RemovePlayer(player);
    }

    void InitItems()
    {
        if (UserManager.Instance.GetItems().Count != 0)
        {
            LoadItemsInfo();
        }
        else
        {
            UserManager.Instance.SaveNewUserData();
        }
    }

    public void LoadItemsInfo()
    {
        var resultItems = UserManager.Instance.GetItems();

        foreach (var item in resultItems)
        {
            if (items.ContainsKey(item.type))
            {
                items[item.type].Level = item.level;
            }
        }
    }

    public float GetBuffValue(string buffType)
    {
        var playerBuff = GetItem(buffType) as PlayerBuff;

        if (playerBuff != null)
        {
            return playerBuff.GetBuffValue();
        }

        return 1f;
    }

    void UpdateDurations()
    {
        itemDurations.UpdateDurations(Time.fixedDeltaTime, 
            (string itemType, float duration, GameObject player) =>
            {
                TryUpdateUI(items[itemType], duration, player);
            }, 
            (string itemType, GameObject player) =>
            {
                TryDestroyUI(items[itemType], player);
            });
    }

    void TryDestroyUI(Item item, GameObject player)
    {
        if (!IsPlayerBot(player))
        {
            itemUIController.DestroySlider(item);
        }
    }

    void TryUpdateUI(Item item, float duration, GameObject player)
    {
        if (!IsPlayerBot(player))
        {
            itemUIController.UpdateSlider(item, duration);
        }
    }

    void TryCreateUI(Item item, GameObject player)
    {
        if (!IsPlayerBot(player))
        {
            itemUIController.CreateSlider(item);
        }
    }

    bool IsPlayerBot(GameObject player)
    {
        return GameInfoManager.Instance.IsPlayerBot(player);
    }
}
