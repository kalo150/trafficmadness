﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class ItemDurations
{
    private Dictionary<GameObject, Dictionary<string, float>> itemTimeLeft;

    public ItemDurations()
    {
        itemTimeLeft = new Dictionary<GameObject, Dictionary<string, float>>();
    }

    public bool IsDurationOver(Item item, GameObject player)
    {
        if (itemTimeLeft.ContainsKey(player))
        {
            return !itemTimeLeft[player].ContainsKey(item.Type);
        }

        return true;
    }

    public void EndDuration(Item item, GameObject player)
    {
        if (itemTimeLeft.ContainsKey(player) && itemTimeLeft[player].ContainsKey(item.Type))
        {
            itemTimeLeft[player].Remove(item.Type);
        }
    }

    public void RemovePlayer(GameObject player)
    {
        itemTimeLeft.Remove(player);
    }

    public void StartDuration(Item item, GameObject player)
    {
        if (itemTimeLeft.ContainsKey(player))
        {
            itemTimeLeft[player].AddOrReplace(item.Type, item.GetFullDuration());
        }
        else
        {
            var value = new Dictionary<string, float>()
            {
                { item.Type, item.GetFullDuration() }
            };

            itemTimeLeft.AddOrReplace(player, value);
        }
    }

    public delegate void OnUpdate(string itemType, float duration, GameObject player);
    public delegate void OnDestroy(string itemType, GameObject player);
    public void UpdateDurations(float updateTime, OnUpdate OnUpdateFunc, OnDestroy OnDestroyFunc)
    {
        foreach (var player in itemTimeLeft)
        {
            foreach (var item in player.Value.ToList())
            {
                if (item.Value <= 0f)
                {
                    itemTimeLeft[player.Key].Remove(item.Key);

                    OnDestroyFunc(item.Key, player.Key);
                }
                else
                {
                    itemTimeLeft[player.Key][item.Key] -= updateTime;

                    OnUpdateFunc(item.Key, item.Value, player.Key);
                }
            }
        }
    }

}
