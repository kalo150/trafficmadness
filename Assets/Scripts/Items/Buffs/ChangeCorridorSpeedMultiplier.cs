﻿using UnityEngine;

public class ChangeCorridorSpeedMultiplier : PlayerBuff
{
    [SerializeField]
    private float bonusEachLevel;

    public override float GetBuffValue()
    {
        return 1f + bonusEachLevel * Level;
    }
}