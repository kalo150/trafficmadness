﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ItemUIController : MonoBehaviour 
{
    private const string thumbnailObject = "Thumbnail";

    [SerializeField]
    private GameObject itemDurationSlider;
    [SerializeField]
    private GameObject slidersParent;
    [SerializeField]
    private float heightOffset;

    private Dictionary<string, GameObject> sliders;
    private Dictionary<string, Slider> durations;

    void Start()
    {
        sliders = new Dictionary<string, GameObject>();
        durations = new Dictionary<string, Slider>();
    }

    public void CreateSlider(Item item)
    {
        if (sliders.ContainsKey(item.Type))
        {
            return;
        }

        var slider = Instantiate(itemDurationSlider);
        slider.transform.SetParent(slidersParent.transform, false);

        var position = itemDurationSlider.transform.position;
        position.y += heightOffset * sliders.Count;
        slider.transform.localPosition = position;

        slider.GetComponentsInChildren<Image>()
            .First(img => img.name == thumbnailObject)
            .sprite = item.Thumbnail;

        sliders.Add(item.Type, slider);
        durations.Add(item.Type, slider.GetComponentInChildren<Slider>());
    }

    public void DestroySlider(Item item)
    {
        string type = item.Type;

        if (sliders.ContainsKey(type))
        {
            var sliderObject = sliders[type];
            var onTopSlider = sliderObject;

            foreach(var slider in sliders)
            {
                if(slider.Value.transform.position.y > onTopSlider.transform.position.y)
                {
                    onTopSlider = slider.Value;
                }
            }

            var tempPosition = onTopSlider.transform.position;
            onTopSlider.transform.position = sliderObject.transform.position;
            sliderObject.transform.position = tempPosition;

            Destroy(sliderObject);
            sliders.Remove(type);
            durations.Remove(type);
        }
    }

    public void UpdateSlider(Item item, float value)
    {
        string type = item.Type;

        if (durations.ContainsKey(type))
        {
            durations[type].value = value / item.GetFullDuration();
        }
    }
}
