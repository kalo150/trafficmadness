﻿using UnityEngine;

public class MainCamera : MonoBehaviour 
{
    [SerializeField]
    private GameObject player;
    [SerializeField]
    private float cameraFollowSpeed;
    [SerializeField]
    private float cameraRotationSpeed;
    [SerializeField]
    private Vector3 playerCameraDistance;
    [SerializeField]
    private Vector3 cameraRotation;

    private PlayerControl playerControl;

    public bool FollowOnlyGrounded { get; set; }

	void Start () 
	{
        FollowOnlyGrounded = true;

        playerControl = player.GetComponent<PlayerControl>();
	}
	
	void Update () 
	{
        if(GameStateManager.Instance.State != GameStates.InGame)
        {
            return;
        }

        Vector3 newDistance = player.transform.position + playerCameraDistance;

        if(!playerControl.IsGrounded() && FollowOnlyGrounded)
        {
            newDistance.y = transform.position.y;
        }

        transform.SetPositionZ(newDistance.z);

        transform.rotation = Quaternion.Euler(cameraRotation);
        transform.position = Vector3.Lerp(transform.position, newDistance, cameraFollowSpeed * Time.deltaTime);
	}
}
