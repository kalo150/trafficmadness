﻿using UnityEngine;

public abstract class PlayerInput : MonoBehaviour
{
    public abstract bool Left();
    public abstract bool Right();
    public abstract bool Slide();
    public abstract bool Jump();
}
