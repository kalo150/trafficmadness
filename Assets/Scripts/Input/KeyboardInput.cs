﻿using UnityEngine;

/// <summary>
/// Read keyboard input.
/// </summary>
public class KeyboardInput : PlayerInput 
{
    private Commands currentCommand;

    private enum Commands
    {
        None,
        Left,
        Right,
        Slide,
        Jump
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.A))
        {
            currentCommand = Commands.Left;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            currentCommand = Commands.Right;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            currentCommand = Commands.Slide;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            currentCommand = Commands.Jump;
        }
    }

    public override bool Left()
    {
        return CheckCommand(Commands.Left);
    }

    public override bool Right()
    {
        return CheckCommand(Commands.Right);
    }

    public override bool Slide()
    {
        return CheckCommand(Commands.Slide);
    }

    public override bool Jump()
    {
        return CheckCommand(Commands.Jump);
    }

    bool CheckCommand(Commands command)
    {
        if(currentCommand == command)
        {
            currentCommand = Commands.None;

            return true;
        }

        return false;
    }
}
