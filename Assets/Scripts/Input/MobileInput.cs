﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Read mobile input.
/// </summary>
public class MobileInput : PlayerInput 
{
    [SerializeField]
    private int touchIndex;
    [SerializeField]
    private float distanceForResponse;

    private delegate void CommandHandler(Touch touch);
    private Dictionary<TouchPhase, CommandHandler> handlers;

    private enum Commands
    {
        None,
        Left, 
        Right,
        Slide,
        Jump
    }

    private Commands currentCommand;
    private bool canProceedCommand;

    private Vector3 touchStartPosition;

    void Awake()
    {
        SetHandlers();
    }

    private void SetHandlers()
    {
        handlers = new Dictionary<TouchPhase, CommandHandler>();

        handlers.Add(TouchPhase.Began, BeganTouchPhase);
        handlers.Add(TouchPhase.Moved, MovedTouchPhase);
        handlers.Add(TouchPhase.Ended, EndedTouchPhase);
    }

    void Update()
    {
        if(Input.touchCount == 0)
        {
            return;
        }

        var touch = Input.GetTouch(touchIndex);

        if(handlers.ContainsKey(touch.phase))
        {
            handlers[touch.phase](touch);
        }

    }

    void BeganTouchPhase(Touch touch)
    {
        touchStartPosition = touch.position;

        canProceedCommand = true;
    }

    void MovedTouchPhase(Touch touch)
    {
        if(!canProceedCommand)
        {
            return;
        }

        float horizontalDifference = touch.position.x - touchStartPosition.x;
        float verticalDifference   = touch.position.y - touchStartPosition.y;

        if (Mathf.Abs(horizontalDifference) > Mathf.Abs(verticalDifference))
        {
            if (horizontalDifference > distanceForResponse)
            {
                SetCommand(Commands.Right);
            }
            else if (horizontalDifference < -distanceForResponse)
            {
                SetCommand(Commands.Left);
            }
        }
        else
        {
            if (verticalDifference > distanceForResponse)
            {
                SetCommand(Commands.Jump);
            }
            else if (verticalDifference < -distanceForResponse)
            {
                SetCommand(Commands.Slide);
            }
        }
    }

    void SetCommand(Commands command)
    {
        currentCommand = command;
        canProceedCommand = false;
    }

    void EndedTouchPhase(Touch touch)
    {
        currentCommand = Commands.None;
    }

    public override bool Left()
    {
        return CheckCommand(Commands.Left);
    }

    public override bool Right()
    {
        return CheckCommand(Commands.Right);
    }

    public override bool Slide()
    {
        return CheckCommand(Commands.Slide);
    }

    public override bool Jump()
    {
        return CheckCommand(Commands.Jump);
    }

    bool CheckCommand(Commands command)
    {
        if(currentCommand == command)
        {
            currentCommand = Commands.None;

            return true;
        }

        return false;
    }
}
