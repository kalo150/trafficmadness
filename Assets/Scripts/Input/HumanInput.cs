﻿using System;
using UnityEngine;

/// <summary>
/// Read player input.
/// </summary>
public class HumanInput : PlayerInput
{
    [SerializeField]
    private PlayerInput keyboardInput;
    [SerializeField]
    private PlayerInput mobileAndroidInput;

    private PlayerInput inputHandler;

    void Awake()
    {
#if UNITY_EDITOR || UNITY_STANDALONE
        inputHandler = keyboardInput;
#elif UNITY_ANDROID
        inputHandler = mobileAndroidInput;
#endif
    }

    public override bool Left()
    {
        return inputHandler.Left();
    }

    public override bool Right()
    {
        return inputHandler.Right();
    }

    public override bool Slide()
    {
        return inputHandler.Slide();
    }

    public override bool Jump()
    {
        return inputHandler.Jump();
    }
}
