﻿public class BotInput : PlayerInput
{
    public bool JumpAction  { set { jump  = value; } }
    public bool LeftAction  { set { left  = value; } }
    public bool RightAction { set { right = value; } }
    public bool SlideAction { set { slide = value; } }

    private bool jump;
    private bool left;
    private bool right;
    private bool slide;

    public override bool Jump()
    {
        return ExecuteCommand(ref jump);
    }

    public override bool Left()
    {
        return ExecuteCommand(ref left);
    }

    public override bool Right()
    {
        return ExecuteCommand(ref right);
    }

    public override bool Slide()
    {
        return ExecuteCommand(ref slide);
    }

    bool ExecuteCommand(ref bool actionState)
    {
        bool actionValue = actionState;

        actionState = false;

        return actionValue;
    }
}