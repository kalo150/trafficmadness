﻿using System.Collections.Generic;
using Facebook.Unity;

public class FacebookSession : UserSession 
{
    private string userName;
    private string userEmail;

    void Awake()
    {
        FB.Init(TryToSetSession);
    }

    void Start()
    {
        TryToSetSession();
    }

    public override bool IsSessionActive()
    {
        return FB.IsLoggedIn;
    }

    public override string GetUserName()
    {
        return userName;
    }

    public override string GetUserEmail()
    {
        return userEmail;
    }

    public override void EndSession()
    {
        FB.LogOut();
    }

    void TryToSetSession()
    {
        if (IsSessionActive())
        {
            SentRequestGetNames();
            SentRequestGetEmail();

            UserManager.Instance.SetUser(this);
        }
    }

    public void LoginWithFacebook()
    {
        var permissions = new List<string>() { "public_profile", "email" };

        FB.LogInWithReadPermissions(permissions, LoginCallback);
    }

    void SentRequestGetNames()
    {
        GraphApiGetCall("first_name,last_name", GetNamesCallback);
    }

    void GetNamesCallback(IGraphResult result)
    {
        if (result.Error == null && IsSessionActive())
        {
            var dict = result.ResultDictionary;

            var firstName = dict["first_name"].ToString();
            var lastName = dict["last_name"].ToString();

            userName = firstName + " " + lastName;
        }
    }

    void SentRequestGetEmail()
    {
        GraphApiGetCall("email", GetEmailCallback);
    }

    void GetEmailCallback(IGraphResult result)
    {
        if (result.Error == null && IsSessionActive())
        {
            var dict = result.ResultDictionary;

            userEmail = dict["email"].ToString();
        }
    }

    void LoginCallback(ILoginResult result)
    {
        TryToSetSession();
    }

    void GraphApiGetCall(string fields, FacebookDelegate<IGraphResult> callback = null)
    {
        var method = HttpMethod.GET;
        var queryString = "/me?fields=" + fields;

        FB.API(queryString, method, callback);
    }
}
