﻿using UnityEngine;

public abstract class UserSession : MonoBehaviour 
{
    public abstract bool IsSessionActive();
    public abstract string GetUserName();
    public abstract string GetUserEmail();
    public abstract void EndSession();
}
