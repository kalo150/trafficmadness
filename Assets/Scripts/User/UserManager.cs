﻿using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Managing user storage, session and database.
/// </summary>
public class UserManager : Singleton<UserManager> 
{
    [SerializeField]
    private bool SyncWithDatabase;
    [SerializeField]
    private bool SyncWithREST;

    private UserSession userSession;
    private UserUIController userUIController;

    private UserData userData_;
    private UserData userData
    {
        get
        {
            if(userData_ == null)
            {
                userData_ = UserData.LoadUser();
            }

            return userData_;
        }
    }

    void Awake()
    {
        userUIController = GetComponent<UserUIController>();
    }

    public void SetUser(UserSession session)
    {
        userSession = session;

        userUIController.UpdateUI();

        Synchronize();
    }

    public bool IsLoggedIn()
    {
        return userSession != null && userSession.IsSessionActive();
    }

    public string GetUserName()
    {
        if(userSession == null)
        {
            return null;
        }

        return userSession.GetUserName();
    }

    public string GetUserEmail()
    {
        if (userSession == null)
        {
            return null;
        }

        return userSession.GetUserEmail();
    }

    public void AddCoins(uint value)
    {
        userData.coins += value;

        userData.Save();
    }

    public uint GetBestScore()
    {
        return userData.score;
    }

    public uint GetCoins()
    {
        return userData.coins;
    }

    public List<Mission> GetMissionProgress()
    {
        return userData.missionProgress ?? new List<Mission>();
    }

    public List<ItemSerializable> GetItems()
    {
        return userData.items ?? new List<ItemSerializable>();
    }

    public void UserLogout()
    {
        if(userSession != null)
        {
            userSession.EndSession();

            userUIController.UpdateUI();
        }

        userSession = null;
    }

    public void SaveNewUserData()
    {
        var newScore = ScoreManager.Instance.Score;
        var coinsEarned = ScoreManager.Instance.Coins;

        userData.items = ItemManager.Instance.SerializableItems().Clone();
        userData.score = GameUtils.MathUtils.Max(newScore, userData.score);
        userData.coins += coinsEarned;
        userData.missionProgress = MissionManager.Instance.ActiveMissions;

        userData.Save();
    }

    public void UpdateUserCoins(uint newValue)
    {
        userData.coins = newValue;
    }

    public void SetBetterUserData(UserData userDataArg)
    {
        userData.coins = GameUtils.MathUtils.Max(userData.coins, userDataArg.coins);
        userData.score = GameUtils.MathUtils.Max(userData.score, userDataArg.score);

        if (userDataArg.items == null)
        {
            return;
        }

        userDataArg.items.Sort((first, second) => first.type.CompareTo(second.type));
        userData.items.Sort((first, second) => first.type.CompareTo(second.type));

        for (int i = 0; i < userDataArg.items.Count; i++)
        {
            if(userDataArg.items[i].type.Equals(userData.items[i].type))
            {
                userData.items[i].level = Mathf.Max(userData.items[i].level, userDataArg.items[i].level);
            }
            else
            {
                break;
            }
        }

        userData.Save();
    }

    void Synchronize()
    {
        if (SyncWithDatabase)
        {
            DatabaseManager.Instance.SynchronizeDatabase();
        }

        if (SyncWithREST)
        {
            REST.Instance.Synchronize();
        }
    }
}
