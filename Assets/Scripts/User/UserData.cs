﻿using System.Collections.Generic;

/// <summary>
/// Serializable player data info.
/// </summary>
[System.Serializable]
public class UserData
{
    public uint score;
    public uint coins;
    public List<Mission> missionProgress;
    public List<ItemSerializable> items;

    public void Save()
    {
        var fileName = ConfigManager.Instance.userData;

        if(!StorageManager.Instance.SerializeClass(fileName, this))
        {
            LogManager.Error("Failed to serialize user data");
        }
    }

    public static UserData LoadUser()
    {
        var fileName = ConfigManager.Instance.userData;

        if(!StorageManager.Instance.FileExists(fileName))
        {
            return new UserData();
        }

        UserData userData;
        if(!StorageManager.Instance.DeserializeClass(fileName, out userData))
        {
            LogManager.Error("Failed to deserialize user data");
        }

        return userData ?? new UserData();
    }
}
