﻿using System;
using UnityEngine;

/// <summary>
/// In editor session testing class.
/// </summary>
public class _EditorUserTestingClass : UserSession
{
    [SerializeField]
    private bool isActiveSession;
    [SerializeField]
    private string userName;
    [SerializeField]
    private string userEmail;

    void Start()
    {
#if UNITY_EDITOR
        if (IsSessionActive())
        {
            UserManager.Instance.SetUser(this);
        }
#endif
    }

    public override bool IsSessionActive()
    {
        return isActiveSession;
    }

    public override string GetUserName()
    {
        return userName;
    }

    public override string GetUserEmail()
    {
        return userEmail;
    }

    public override void EndSession()
    {
        isActiveSession = false;
    }
}
