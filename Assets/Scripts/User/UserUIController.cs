﻿using UnityEngine;
using UnityEngine.UI;

public class UserUIController : MonoBehaviour
{
    [SerializeField]
    private GameObject loginOptions;
    [SerializeField]
    private GameObject usernameText;
    [SerializeField]
    private GameObject usernameField;

    private bool loginUIUpdated;

    void Start()
    {
        loginUIUpdated = false;
    }

    void FixedUpdate()
    {
        if (GameStateManager.Instance.State != GameStates.Menu)
        {
            return;
        }

        if (!loginUIUpdated)
        {
            UpdateInfoAndUI();
        }
    }

    public void UpdateInfoAndUI()
    {
        if (UserManager.Instance.IsLoggedIn())
        {
            LoginUser();
        }
        else
        {
            LogoutUser();
        }
    }

    public void UpdateUI()
    {
        loginUIUpdated = false;
    }

    void LoginUser()
    {
        loginOptions.SetActive(false);

        var userName = UserManager.Instance.GetUserName();
        bool shouldUpdate = !string.IsNullOrEmpty(userName);

        if (shouldUpdate)
        {
            var text = usernameText.GetComponent<Text>();
            text.text = userName;

            usernameField.SetActive(true);
            loginUIUpdated = true;
        }
    }

    void LogoutUser()
    {
        loginOptions.SetActive(true);
        usernameField.SetActive(false);
        loginUIUpdated = true;
    }
}
