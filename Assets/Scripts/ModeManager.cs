﻿using System.Collections.Generic;
using UnityEngine;

public class ModeManager : Singleton<ModeManager> 
{
    private const string currentModeCache = "Current Mode";

    public enum Mode
    {
        FreeRun,
        Competitive,
        Multiplayer
    }

    private delegate void ModeHandler();

    private Dictionary<Mode, ModeHandler> handlers;
    private Mode currentMode;

    [SerializeField]
    private GameObject compatitiveModeElements;

    public Mode CurrentMode { get { return currentMode; } }

    void Awake()
    {
        handlers = new Dictionary<Mode, ModeHandler>()
        {
            { Mode.FreeRun, HandleFreeMode },
            { Mode.Competitive, HandleCompetitiveMode },
            { Mode.Multiplayer, HandleMultiplayerMode }
        };

        var cachedMode = CacheManager.Instance.GetValue(currentModeCache);

        SetMode((Mode) (cachedMode ?? Mode.FreeRun));
    }

    public void SetMode(Mode mode)
    {
        ResetModeElements();

        handlers[mode]();

        currentMode = mode;

        CacheManager.Instance.SaveValue(currentModeCache, currentMode);
    }

    void HandleFreeMode()
    {
        ScoreManager.Instance.CountScore = true;
    }

    void HandleCompetitiveMode()
    {
        compatitiveModeElements.SetActive(true);
        
        ScoreManager.Instance.CountScore = false;
    }

    void HandleMultiplayerMode()
    {
        MultiplayerManager.Instance.ActiveMultiplayer();

        ScoreManager.Instance.CountScore = false;
    }

    void ResetModeElements()
    {
        compatitiveModeElements.SetActive(false);

        MultiplayerManager.Instance.DeactiveMultiplayer();
    }
}