﻿/// <summary>
/// Track player activity.
/// </summary>
public class TrackManager : Singleton<TrackManager>
{
    public int LaneChanges   { get; private set; }
    public int ItemCollected { get; private set; }
    public int Slides        { get; private set; }
    public int Jumps         { get; private set; }

    void Start()
    {
        LaneChanges = 0;
        ItemCollected = 0;
        Slides = 0;
        Jumps = 0;
    }

    public void MoveLeft()
    {
        LaneChanges++;
    }

    public void MoveRight()
    {
        LaneChanges++;
    }

    public void CollectItem()
    {
        ItemCollected++;
    }

    public void Slide()
    {
        Slides++;
    }

    public void Jump()
    {
        Jumps++;
    }
}