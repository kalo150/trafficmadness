﻿using UnityEngine;

public class AudioManager : StrictSingleton<AudioManager> 
{
    private const string soundStateCacheName = "SoundState";

    private bool isSoundOn;
    private AudioSource audioSource;

    new void Awake()
    {
        base.Awake();

        audioSource = GetComponent<AudioSource>();

        var cachedState = CacheManager.Instance.GetValue(soundStateCacheName);

        IsSoundOn = (bool) (cachedState ?? true);
    }

    public bool IsSoundOn
    {
        get
        {
            return isSoundOn;
        }

        set
        {
            isSoundOn = value;

            foreach (var audioSource in GetComponentsInChildren<AudioSource>())
            {
                audioSource.enabled = isSoundOn;
            }

            CacheManager.Instance.SaveValue(soundStateCacheName, isSoundOn);
        }
    }

    public void PlayAudioClip(AudioClip audioClip)
    {
        if(IsSoundOn)
        {
            audioSource.PlayOneShot(audioClip);
        }
    }
}