﻿/// <summary>
/// Singleton only for GameObjects, can not be created new instances.
/// Is not being destroyed on scene load.
/// </summary>
/// <typeparam name="T">Child class reference</typeparam>
public class StrictSingleton<T> : Singleton<T> where T : Singleton<T>
{
	public virtual void Awake()
    {
        if(Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}
