﻿using UnityEngine;

/// <summary>
/// Singleton only for GameObjects, can not be created new instances.
/// </summary>
/// <typeparam name="T">Child class reference</typeparam>
public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    private static T _instance = null;
    
    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<T>();
            }
            return _instance;
        }
    }

}
