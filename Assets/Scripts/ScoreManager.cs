﻿using UnityEngine;
using System;

public class ScoreManager : Singleton<ScoreManager>
{
    [SerializeField]
    private float scoreMultiplier;
    private float lastPlayerPosition;
    private bool  countScore;

    private uint coins;
    private float score;

    public bool CountScore
    {
        get { return countScore;  }
        set { countScore = value; }
    }

    public float ScoreMultiplier
    {
        get { return scoreMultiplier;  }
        set { scoreMultiplier = value; }
    }
    public uint Score { get { return (uint) score; } }
    public uint Coins { get { return coins; } }

    void Start()
    {
        score = 0f;
        coins = 0;
    }

    void FixedUpdate()
    {
        if (GameStateManager.Instance.State != GameStates.InGame || !countScore)
        {
            return;
        }

        AddScore();
    }

    public void AddCoin(uint value)
    {
        coins += value;
    }

    public uint GetBestScore()
    {
        return UserManager.Instance.GetBestScore();
    }

    void AddScore()
    {
        float positionDifferences = GameInfoManager.Instance.LocalPlayer.z - lastPlayerPosition;
        lastPlayerPosition = GameInfoManager.Instance.LocalPlayer.z;

        float scoreFromPosition = Math.Max(positionDifferences, 0f);
        score += scoreFromPosition * scoreMultiplier;
    }
}
