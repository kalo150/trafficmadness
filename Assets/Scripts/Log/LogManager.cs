﻿using UnityEngine;

public class LogManager : Singleton<LogManager> 
{
    [SerializeField]
    private bool enableError;
    [SerializeField]
    private bool enableWarning;
    [SerializeField]
    private bool enableLog;

    private ToastMessage toastMessage;

    void Awake()
    {
        toastMessage = GetComponent<ToastMessage>();
    }

    public static void Error(string message)
    {
        if(Instance.enableError)
        {
            ShowMessage(message);
        }
    }

    public static void Warning(string message)
    {
        if (Instance.enableWarning)
        {
            ShowMessage(message);
        }
    }

    public static void Log(string message)
    {
        if (Instance.enableLog)
        {
            ShowMessage(message);
        }
    }

    static void ShowMessage(string message)
    {
#if UNITY_EDITOR
        Debug.Log(message);
#elif UNITY_ANDROID
        Instance.toastMessage.SetMessage(message);
#endif
    }
}
