﻿using UnityEngine;
using System.Collections;

public class ToastMessage : MonoBehaviour 
{
    [SerializeField]
    private float messageDuration;
    [SerializeField]
    private float rectWidth;
    [SerializeField]
    private bool widthMatchScreenSize;
    [SerializeField]
    private float rectHeight;
    
    private string message;
    private float durationLeft;

    void Awake()
    {
        rectWidth = widthMatchScreenSize ? Screen.width : rectWidth;
    }

    public void SetMessage(string message)
    {
        this.message = message;
        durationLeft = messageDuration;
    }

    void OnGUI()
    {
        if (durationLeft <= 0f)
        {
            return;
        }

        float xPosition = (Screen.width - rectWidth) / 2f;

        var guiStyle = new GUIStyle(GUI.skin.button);
        guiStyle.fontSize = 20;

        GUI.Label(
            new Rect(xPosition, 0, rectWidth, rectHeight), 
            message, 
            guiStyle
            );

        durationLeft -= Time.deltaTime;
    }
}
