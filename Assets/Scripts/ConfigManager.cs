﻿public class ConfigManager : Singleton<ConfigManager>
{
    // Game save files
    public string userData;
    public string cachedDataFile;

    // Database
    public string playerInfoTableDB;
    public string playerInfoTableDB_ID;
    public string playerInfoTableDB_EmailField;
    public string playerInfoTableDB_ScoreField;
    public string playerInfoTableDB_NameField;

    public string itemInfoTableDB;
    public string itemInfoTableDB_TypeField;
    public string itemInfoTableDB_LevelField;
    public string itemInfoTableDB_playerInfoID;
}