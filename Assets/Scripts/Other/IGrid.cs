﻿using System.Collections.Generic;

public abstract class IGrid<T>
{
    public abstract bool Empty();
    public abstract int CountRows();
    public abstract int CountCols();
    public abstract void Add(List<T> row);
    public abstract void AddFront(List<T> row);
    public abstract List<T> GetLastRow();
}
