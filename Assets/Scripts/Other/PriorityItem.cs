﻿using UnityEngine;

public class PriorityItem
{
    [SerializeField]
    private int priority;

    public int Priority { get { return priority; } }
}
