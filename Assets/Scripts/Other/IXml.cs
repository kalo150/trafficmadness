﻿public interface IXml
{
    string ToXml();
    void FromXml(string xmlContent);
}
