﻿using System.Collections.Generic;

public class Grid<T> : IGrid<T>
{
    const int defaultGridSize = 1;

    private List<List<T>> grid;

    public List<T> this[int y]
    {
        get { return grid[y]; }
        set { grid[y] = value; }
    }

    public T this[int y, int x]
    {
        get { return grid[y][x]; }
        set { grid[y][x] = value; }
    }

    public Grid(int gridSizeX = defaultGridSize, int gridSizeY = defaultGridSize, T defaultValue = default(T))
    {
        grid = GameUtils.ArrayUtils.CreateList<List<T>>(gridSizeY);
        for(int i = 0; i < gridSizeY; i++)
        {
            grid[i] = GameUtils.ArrayUtils.CreateList<T>(gridSizeX, defaultValue);
        }
    }

    public override bool Empty()
    {
        return grid.Count == 0;
    }

    public override int CountRows()
    {
        return grid.Count;
    }

    public override int CountCols()
    {
        return grid[0].Count;
    }

    public override void Add(List<T> row)
    {
        var copyList = row.Clone();

        grid.Add(copyList);
    }

    public override void AddFront(List<T> row)
    {
        var copyList = row.Clone();

        grid.Insert(0, copyList);
    }

    public override List<T> GetLastRow()
    {
        return grid[grid.Count - 1];
    }

    public bool ValidIndex(int x, int y)
    {
        return x >= 0 && y >= 0 && x < CountCols() && y < CountRows();
    }

    public bool Concat(Grid<T> otherGrid)
    {
        if(otherGrid.CountCols() != CountCols())
        {
            return false;
        }

        for(int i = 0; i < otherGrid.CountRows(); i++)
        {
            Add(otherGrid.grid[i]);
        }

        return true;
    }

    public bool Prepend(Grid<T> otherGrid)
    {
        if (otherGrid.CountCols() != CountCols())
        {
            return false;
        }

        var newGrid = new List<List<T>>(grid.Count + otherGrid.grid.Count);

        for (int i = 0; i < otherGrid.CountRows(); i++)
        {
            newGrid.Add(otherGrid.grid[i]);
        }

        for (int i = 0; i < CountRows(); i++)
        {
            newGrid.Add(grid[i]);
        }

        grid = newGrid;

        return true;
    }
}
