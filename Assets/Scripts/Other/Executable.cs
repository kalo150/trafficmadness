﻿using UnityEngine; 

public interface Executable
{
    bool Execute(GameObject player);
}