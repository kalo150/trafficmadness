﻿using UnityEngine;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

public class DatabaseSync : MonoBehaviour 
{
    private const char itemSeparator = ';';

    private UserData userData;
    private Int32 playerId;
    private bool playerInfoUpdating;
    private Task currentTask;
    private IDictionary<string, object> playerDataResult;

    public void StartSynchronization()
    {
        if (!UserManager.Instance.IsLoggedIn())
        {
            return;
        }

        GetPlayerInfoRequest();
    }

    void FixedUpdate()
    {
        if (!IsRunnningRequest())
        {
            return;
        }

        if (RequestFailed())
        {
            DisableSync();
        }
        else if (IsGetPlayerInfoCompleted())
        {
            UpdatePlayerInfo(playerDataResult);

            SavePlayerInfo();

            GetItemsRequest();
        }
        else if (IsGetItemsCompeleted())
        {
            UpdatePlayerItems(playerDataResult);

            SavePlayerItems();

            DisableSync();
        }
    }

    bool IsRunnningRequest()
    {
        return currentTask != null;
    }

    void DisableSync()
    {
        currentTask = null;
    }

    bool IsGetItemsCompeleted()
    {
        return currentTask.IsCompleted;
    }

    bool IsGetPlayerInfoCompleted()
    {
        return currentTask.IsCompleted && playerInfoUpdating;
    }

    bool RequestFailed()
    {
        return currentTask.IsFaulted || currentTask.IsCanceled;
    }

    void GetPlayerInfoRequest()
    {
        var emailField = ConfigManager.Instance.playerInfoTableDB_EmailField;
        var email = UserManager.Instance.GetUserEmail();
        var table = ConfigManager.Instance.playerInfoTableDB;

        currentTask = DatabaseManager.Instance.GetData(table, emailField, email,
        (IDictionary<string, object> result) =>
        {
            playerDataResult = result;
        });

        playerInfoUpdating = true;
    }

    void GetItemsRequest()
    {
        var playerIdField = ConfigManager.Instance.playerInfoTableDB_ID;
        var playerForeignField = ConfigManager.Instance.itemInfoTableDB_playerInfoID;
        playerId = (Int32)playerDataResult[playerIdField];
        var table = ConfigManager.Instance.itemInfoTableDB;

        currentTask = DatabaseManager.Instance.GetData(table, playerForeignField, playerId.ToString(),
        (IDictionary<string, object> result) =>
        {
            playerDataResult = result;
        });

        playerInfoUpdating = false;
    }

    public void UpdatePlayerItems(IDictionary<string, object> result)
    {
        var typeField  = ConfigManager.Instance.itemInfoTableDB_TypeField;
        var levelField = ConfigManager.Instance.itemInfoTableDB_LevelField;

        if (!result.ContainsKey(typeField))
        {
            return;
        }

        string[] newType  = ((string)result[typeField]).Split(itemSeparator);
        string[] newLevel = ((string)result[levelField]).Split(itemSeparator);

        var userData = new UserData();
        userData.items = new List<ItemSerializable>(newType.Length);

        for(int i = 0; i < newType.Length; i++)
        {
            if(newType[i].Equals(string.Empty))
            {
                break;
            }

            userData.items.Add(new ItemSerializable()
            {
                type = newType[i],
                level = int.Parse(newLevel[i])
            });
        }

        UserManager.Instance.SetBetterUserData(userData);
    }

    public void SavePlayerItems()
    {
        if (!UserManager.Instance.IsLoggedIn())
        {
            return;
        }

        var items = UserManager.Instance.GetItems();

        var typeField     = ConfigManager.Instance.itemInfoTableDB_TypeField;
        var levelField    = ConfigManager.Instance.itemInfoTableDB_LevelField;
        var playerIdField = ConfigManager.Instance.itemInfoTableDB_playerInfoID;

        var table = ConfigManager.Instance.itemInfoTableDB;

        string typeString  = string.Empty;
        string levelString = string.Empty;

        foreach (var item in items)
        {
            typeString += item.type.ToString() + itemSeparator;
            levelString += item.level.ToString() + itemSeparator;
        };

        var playerItems = new Dictionary<string, object>()
        {
            { typeField    , typeString          },
            { levelField   , levelString         },
            { playerIdField, playerId.ToString() }
        };

        currentTask = DatabaseManager.Instance.SaveData(table, playerIdField, playerId.ToString(), playerItems);
    }

    public void UpdatePlayerInfo(IDictionary<string, object> result)
    {
        var scoreField = ConfigManager.Instance.playerInfoTableDB_ScoreField;

        if (!result.ContainsKey(scoreField))
        {
            return;
        }

        var newScore = Convert.ToUInt32(result[scoreField]);

        var userData = new UserData();
        userData.score = newScore;
        UserManager.Instance.SetBetterUserData(userData);
    }

    void SavePlayerInfo()
    {
        if (!UserManager.Instance.IsLoggedIn())
        {
            return;
        }

        var email = UserManager.Instance.GetUserEmail();
        var name  = UserManager.Instance.GetUserName();
        var score = ScoreManager.Instance.GetBestScore();

        var emailField = ConfigManager.Instance.playerInfoTableDB_EmailField;
        var nameField  = ConfigManager.Instance.playerInfoTableDB_NameField;
        var scoreField = ConfigManager.Instance.playerInfoTableDB_ScoreField;

        var table = ConfigManager.Instance.playerInfoTableDB;

        var data = new Dictionary<string, object>()
        {
            { emailField, email },
            { nameField , name  },
            { scoreField, score }
        };

        DatabaseManager.Instance.SaveData(table, emailField, email, data);
    }
}
