﻿using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

/// <summary>
/// Managing file storage.
/// </summary>
public class StorageManager : StrictSingleton<StorageManager> 
{
    private string gameStoragePath
    {
        get
        {
            return Application.persistentDataPath;
        }   
    }

    public bool SaveData(string fileName, string data)
    {
        string fullPath = gameStoragePath + fileName;

        try
        {
            GameUtils.FileUtils.Write(fullPath, data);
        }
        catch(Exception e)
        {
            LogManager.Error(e.Message);

            return false;
        }

        return true;
    }

    public bool GetData(string fileName, out string data)
    {
        string fullPath = gameStoragePath + fileName;

        try
        {
            data = GameUtils.FileUtils.Read(fullPath);
        }
        catch(Exception e)
        {
            LogManager.Error(e.Message);
            data = null;

            return false;
        }

        return data != null;
    }

    public bool Delete(string fileName)
    {
        string fullPath = gameStoragePath + fileName;

        try
        {
            GameUtils.FileUtils.Delete(fullPath);
        }
        catch(Exception e)
        {
            LogManager.Error(e.Message);

            return false;
        }

        return true;
    }

    public bool SerializeClass<T>(string fileName, T objClass) where T : class
    {
        string fullPath = gameStoragePath + fileName;

        try
        {
            var formatter = new BinaryFormatter();
            var stream = new FileStream(fullPath, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, objClass);
            stream.Close();
        }
        catch (Exception e)
        {
            LogManager.Error(e.Message);

            return false;
        }

        return true;
    }

    public bool DeserializeClass<T>(string fileName, out T objClass) where T : class
    {
        string fullPath = gameStoragePath + fileName;

        try
        {
            var formatter = new BinaryFormatter();
            var stream = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.Read);
            objClass = (T) formatter.Deserialize(stream);
            stream.Close();
        }
        catch(Exception e)
        {
            LogManager.Error(e.Message);

            objClass = null;

            return false;
        }

        return true;
    }

    public bool LoadFromResource(string fileName, out UnityEngine.Object resource)
    {
        resource = Resources.Load(fileName);

        if(resource == null)
        {
            LogManager.Error("Error occurred when trying to load resource: " + fileName);

            return false;
        }

        return true;
    }

    public bool FileExists(string fileName)
    {
        return File.Exists(gameStoragePath + fileName);
    }
}
