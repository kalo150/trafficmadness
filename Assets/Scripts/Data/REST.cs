﻿using UnityEngine;
using System.Net;
using System.IO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

public class REST : StrictSingleton<REST>
{
    private const string contentType = "application/json";
    private const string postRequest = "POST";
    private const string putReqeust  = "PUT";

    private const string emailJson = "email";
    private const string nameJson  = "name" ;
    private const string itemsJson = "items";
    private const string scoreJson = "score";
    private const string typeJson  = "type" ;
    private const string levelJson = "level";
    private const string idJson    = "id"   ;

    [SerializeField]
    private string restfulUrl;

    public object JsonConvert { get; private set; }

    private enum RequestStatus
    {
        Undefined,
        GetDataCompleted,
        SaveDataCompleted
    }

    private RequestStatus requestStatus;
    private UserData userDataRequest;
    private bool isRequestEmptyResult;
    private long userId;

    void FixedUpdate()
    {
        switch(requestStatus)
        {
            case RequestStatus.GetDataCompleted:

                if(!isRequestEmptyResult)
                {
                    UserManager.Instance.SetBetterUserData(userDataRequest);
                }

                requestStatus = RequestStatus.Undefined;
                userDataRequest = null;
                SaveDataWeb();

                break;
            case RequestStatus.SaveDataCompleted:

                requestStatus = RequestStatus.Undefined;

                break;
        }
    }

    public void Synchronize()
    {
        GetDataFromWeb();
    }

    void GetDataFromWeb()
    {
        var fullUrl = restfulUrl + "/" + UserManager.Instance.GetUserEmail();

        Task.Run(() =>
        {
            var request = WebRequest.Create(fullUrl) as HttpWebRequest;
            request.Accept = contentType;
            var response = request.GetResponse() as HttpWebResponse;

            if(response.StatusCode == HttpStatusCode.OK)
            {
                var dataStream = response.GetResponseStream();
                var reader = new StreamReader(dataStream);

                string json = reader.ReadToEnd();

                var result = new JSONObject(json);

                var newScore = Convert.ToUInt32(result[scoreJson].i);

                var userData = new UserData();
                userData.score = newScore;

                var items = result[itemsJson].list;
                userData.items = new List<ItemSerializable>(items.Count);

                foreach (var item in items)
                {
                    userData.items.Add(new ItemSerializable()
                    {
                        type = item[typeJson].str,
                        level = (int)item[levelJson].i
                    });
                };

                isRequestEmptyResult = result[emailJson].IsNull;
                userId = result[idJson].i;

                userDataRequest = userData;

                requestStatus = RequestStatus.GetDataCompleted;

                reader.Close();
            }

            response.Close();
        });
    }

    void SaveDataWeb()
    {
        var finalJsonObject = new JSONObject();
        finalJsonObject.AddField(nameJson , UserManager.Instance.GetUserName());
        finalJsonObject.AddField(emailJson, UserManager.Instance.GetUserEmail());
        finalJsonObject.AddField(scoreJson, UserManager.Instance.GetBestScore());

        var items = UserManager.Instance.GetItems();

        JSONObject[] jsonItemsObjects = new JSONObject[items.Count];

        for(int i = 0; i < items.Count; i++)
        {
            jsonItemsObjects[i] = new JSONObject();
            jsonItemsObjects[i].AddField(typeJson , items[i].type);
            jsonItemsObjects[i].AddField(levelJson, items[i].level);
        }

        finalJsonObject.AddField(itemsJson, new JSONObject(jsonItemsObjects));

        requestStatus = RequestStatus.SaveDataCompleted;

        var body = finalJsonObject.ToString();

        if(isRequestEmptyResult)
        {
            SendSaveRequest(restfulUrl, body, postRequest);
        }
        else
        {
            var putUrl = restfulUrl + "/" + userId;

            SendSaveRequest(putUrl, body, putReqeust);
        }
    }

    void SendSaveRequest(string fullUrl, string body, string method)
    {
        Task.Run(() =>
        {
            WebRequest request = WebRequest.Create(fullUrl);
            request.Method = method;
            var byteArray = System.Text.Encoding.UTF8.GetBytes(body);
            request.ContentType = contentType;
            request.ContentLength = byteArray.Length;
            var dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
        });
    }
}
