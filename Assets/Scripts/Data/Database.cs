﻿using UnityEngine;
using System.Collections.Generic;
using System.Threading.Tasks;

public abstract class Database : MonoBehaviour 
{
    public delegate void GetEntryCallback(IDictionary<string, object> result);

    public abstract Task GetData(string tableName, string field, string value, GetEntryCallback callback);
    public abstract Task SaveData(string tableName, string field, string value, IDictionary<string, object> data);
}
