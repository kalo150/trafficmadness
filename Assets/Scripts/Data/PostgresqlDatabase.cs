﻿using UnityEngine;
using System.Threading.Tasks;
using System.Collections.Generic;
using System;
using UnityNpgsql;
using System.Linq;

public class PostgresqlDatabase : Database 
{
    [Serializable]
    private class PostgresqlConnectionArgs
    {
        public string key;
        public string value;
    }

    [SerializeField]
    private List<PostgresqlConnectionArgs> postgresqlConnectionArgs;

    private string connectionInfo;

    void Awake()
    {
        connectionInfo = string.Empty;

        foreach(var args in postgresqlConnectionArgs)
        {
            connectionInfo += String.Format("{0}={1};", args.key, args.value);
        }
    }

    public override Task SaveData(string tableName, string field, string value, IDictionary<string, object> data)
    {
        Task task = Task.Run(() =>
        {
            using (var conn = new NpgsqlConnection(connectionInfo))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    cmd.Connection = conn;
                    cmd.CommandText = String.Format("SELECT COUNT(*) FROM {0} WHERE {1}='{2}'", tableName, field, value);

                    if((Int64) cmd.ExecuteScalar() == 0)
                    {
                        cmd.CommandText = FormatInsertRequest(data, tableName);
                    }
                    else
                    {
                        cmd.CommandText = FormatUpdateRequest(data, tableName, field, value);
                    }


                    cmd.ExecuteNonQuery();
                }
            }
        });

        return task;
    }

    public override Task GetData(string tableName, string field, string value, GetEntryCallback callback)
    {
        Task task = Task.Run(() =>
        {
            using (var conn = new NpgsqlConnection(connectionInfo))
            {
                conn.Open();
                using (var cmd = new NpgsqlCommand())
                {
                    Dictionary<string, object> resultDict = null;

                    cmd.Connection = conn;
                    cmd.CommandText = String.Format("SELECT * FROM {0} WHERE {1}='{2}'", tableName, field, value);

                    var result = cmd.ExecuteReader();

                    resultDict = new Dictionary<string, object>();

                    if (result.Read())
                    {
                        for (int i = 0; i < result.FieldCount; i++)
                        {
                            resultDict[result.GetName(i)] = result[i];
                        }
                    }

                    callback(resultDict);
                }
            }
        });

        return task;
    }

    string FormatInsertRequest(IDictionary<string, object> data, string tableName)
    {
        var keyArray = data.Keys.ToArray();
        var valueArray = data.Values
            .Select((x) => "'" + x.ToString() + "'")
            .ToArray();

        var fieldNames = string.Join(",", keyArray);
        var fieldValues = string.Join(",", valueArray);

        return String.Format("INSERT INTO {0} ({1}) VALUES ({2})", tableName, fieldNames, fieldValues);
    }

    string FormatUpdateRequest(IDictionary<string, object> data, string tableName, string field, string value)
    {
        var listData = new string[data.Count];

        for (int i = 0; i < data.Count; i++)
        {
            var element = data.ElementAt(i);
            listData[i] = String.Format("{0}='{1}'", element.Key, element.Value);
        }

        var formatedData = string.Join(",", listData);


        return String.Format("UPDATE {0} SET {1} WHERE {2}='{3}'", tableName, formatedData, field, value);
    }
}
