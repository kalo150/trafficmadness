﻿using UnityEngine;
using System.Collections.Generic;
using System.Threading.Tasks;

public class DatabaseManager : Singleton<DatabaseManager> 
{
    [SerializeField]
    private Database database;

    private DatabaseSync databaseSync;

    void Awake()
    {
        databaseSync = GetComponent<DatabaseSync>();
    }

    public Task GetData(string tableName, string field, string value, Database.GetEntryCallback callback)
    {
        return database.GetData(tableName, field, value, callback);
    }

    public Task SaveData(string tableName, string field, string value, IDictionary<string, object> data)
    {
        return database.SaveData(tableName, field, value, data);
    }

    public void SynchronizeDatabase()
    {
        databaseSync.StartSynchronization();
    }
}
