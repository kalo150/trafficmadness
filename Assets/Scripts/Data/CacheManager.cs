﻿using System.Collections.Generic;

public class CacheManager : StrictSingleton<CacheManager>
{
    private Dictionary<string, object> cachedData;

    private Dictionary<string, object> CachedData
    {
        get
        {
            if(cachedData == null)
            {
                LoadData();
            }

            return cachedData;
        }
    }

    public object GetValue(string name)
    {
        if(CachedData.ContainsKey(name))
        {
            return CachedData[name];
        }

        return null;
    }

    public void SaveValue(string name, object data)
    {
        cachedData.AddOrReplace(name, data);

        SaveData();
    }

    public void DeleteValue(string name)
    {
        cachedData.Remove(name);

        SaveData();
    }

    void LoadData()
    {
        var fileName = ConfigManager.Instance.cachedDataFile;

        if(!StorageManager.Instance.FileExists(fileName))
        {
            cachedData = new Dictionary<string, object>();

            return;
        }

        if(!StorageManager.Instance.DeserializeClass(fileName, out cachedData))
        {
            LogManager.Error("Was not able to load cached data from file " + fileName);
        }
    }

    void SaveData()
    {
        var fileName = ConfigManager.Instance.cachedDataFile;

        if (!StorageManager.Instance.SerializeClass(fileName, cachedData))
        {
            LogManager.Error("Was not able to save cached data from file " + fileName);
        }
    }
}
