﻿using UnityEngine;

public class VehicleMoving : MonoBehaviour 
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private float distanceFromPlayerStartMoving;
	
	void FixedUpdate () 
	{
        if(GameStateManager.Instance.State != GameStates.InGame)
        {
            return;
        }

        if (ShouldMove())
        {
            var newPosition = transform.position.z - speed * Time.fixedDeltaTime;

            transform.SetPositionZ(newPosition);
        }
	}

    bool ShouldMove()
    {
        float movingPosition = GameInfoManager.Instance.FirstPlayer.z + distanceFromPlayerStartMoving;

        return transform.position.z < movingPosition;
    }
}
