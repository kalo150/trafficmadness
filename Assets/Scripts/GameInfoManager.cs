﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class GameInfoManager : Singleton<GameInfoManager> 
{
    private const string playerTag = "Player";

    [SerializeField]
    private GameObject localPlayer;

    [SerializeField]
    private List<GameObject> players;

    public List<GameObject> Players
    {
        get
        {
            return players;
        }
    }

    public int PlayerCount { get { return players.Count; } }

	public Vector3 FirstPlayer
    {
        get
        {
            if (Players.Count == 0)
            {
                return Vector3.zero;
            }

            if (ModeManager.Instance.CurrentMode == ModeManager.Mode.FreeRun)
            {
                return Players.First(player => !IsPlayerBot(player)).transform.position;
            }

            Players.Sort(SortByPositionPredicate);

            return Players.First().transform.position;
        }
    }

    public Vector3 LastPlayer
    {
        get
        {
            if (Players.Count == 0)
            {
                return Vector3.zero;
            }

            if (ModeManager.Instance.CurrentMode == ModeManager.Mode.FreeRun)
            {
                return Players.First(player => !IsPlayerBot(player)).transform.position;
            }

            Players.Sort(SortByPositionPredicate);

            return Players.Last().transform.position;
        }
    }

    public Vector3 LocalPlayer
    {
        get
        {
            return localPlayer.transform.position;    
        }
    }

    public void AddPlayer(GameObject player)
    {
        Players.Add(player);
    }

    public void RemovePlayer(GameObject player)
    {
        Players.Remove(player);
    }

    public int SortByPositionPredicate(GameObject first, GameObject second)
    {
        return first.transform.position.z.CompareTo(second.transform.position.z);
    }

    public bool IsPlayerBot(GameObject player)
    {
        return player.layer == LayerMask.NameToLayer("Bot");
    }
}
