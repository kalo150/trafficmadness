﻿using UnityEngine;

public class ObjectWithLifespan : MonoBehaviour 
{
    [SerializeField]
    private float secondsLife;

	void Start () 
	{
        Invoke("DestroySelf", secondsLife);
	}

    void DestroySelf()
    {
        Destroy(gameObject);
    }
}   
