﻿using UnityEngine;

public class PlayerAnimationController : MonoBehaviour 
{
    private const string jumpTrigger  = "Jump" ;
    private const string slideTrigger = "Slide";
    private const string leftTrigger  = "Left" ;
    private const string rightTrigger = "Right";
    private const string runTrigger   = "Run"  ;
    private const string swimTrigger  = "Swim" ;
    private const string idleTrigger  = "Idle" ;
    private const string deadTrigger  = "Dead" ;

    public enum AnimationState
    {
        NotMoving,
        Running,
        Swimming
    }

    private Animator animator;
    private AnimationState animationState;

    public AnimationState CurrentAnimationState
    {
        get { return animationState;  } 
        set { animationState = value; }
    }

    private GameStates prevGameState;

	void Awake() 
	{
        animator = GetComponent<Animator>();
	}

    void Update()
    {
        var gameState = GameStateManager.Instance.State;

        if (gameState == prevGameState)
        {
            return;
        }

        OnGameStateChange(gameState);

        prevGameState = gameState;
    }

    void OnGameStateChange(GameStates state)
    {
        switch(state)
        {
            case GameStates.Menu    : Idle()  ; break;
            case GameStates.InGame  : InGame(); break;
            case GameStates.Pause   : Pause() ; break;
            case GameStates.GameOver: Dead()  ; break;
        }
    }

    public void Slide()
    {
        if (animationState == AnimationState.Running)
        {
            animator.SetTrigger(slideTrigger);
        }
    }

    public void Pause()
    {
        animator.enabled = false;
    }

    public void InGame()
    {
        if(animator.isActiveAndEnabled)
        {
            Run();
        }
        else
        {
            animator.enabled = true;
        }
    }

    public void Jump()
    {
        if (animationState == AnimationState.Running)
        {
            animator.SetTrigger(jumpTrigger);
        }
    }

    public void Left()
    {
        if (animationState == AnimationState.Running)
        {
            animator.SetTrigger(leftTrigger);
        }
    }

    public void Right()
    {
        if (animationState == AnimationState.Running)
        {
            animator.SetTrigger(rightTrigger);
        }
    }

    public void Run()
    {
        animationState = AnimationState.Running;

        animator.SetTrigger(runTrigger);
    }

    public void Swim()
    {
        animationState = AnimationState.Swimming;

        animator.SetTrigger(swimTrigger);
    }

    public void Idle()
    {
        animationState = AnimationState.NotMoving;

        animator.SetTrigger(idleTrigger);
    }

    public void Dead()
    {
        animationState = AnimationState.NotMoving;

        if (!GameInfoManager.Instance.IsPlayerBot(gameObject))
        {
            animator.SetTrigger(deadTrigger);
        }
    }
}
